module.exports = {
    apps : [
        {
          name: "api",
          script: "./dist/src/main.js",
          watch: true,
          env_production: {
              "NODE_ENV": "production",
          }
        }
    ]
  }