import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppConfig, DatabaseConfig } from './config';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { ChatModule } from './modules/chat/chat.module';
import { ChatEventGateway } from './modules/events/chat-event/chat-event.gateway';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { AuthGuard } from './modules/auth/auth.guard';
import { RolesGuard } from './modules/auth/auth-role.guard';
import { RightsModule } from './modules/rights/rights.module';
import { MailModule } from './modules/mail/mail.module';
import { BullModule } from '@nestjs/bull';
import { ChatConsumer } from './queue/chats.queue';
import { AuthInterceptor } from './modules/auth/auth.interceptor';
import { CategoryModule } from './modules/category/category.module';
import { TypeCategorieModule } from './modules/type-categorie/type-categorie.module';
import { OptionTypeCategorieServiceModule } from './modules/option-type-categorie-service/option-type-categorie-service.module';
import { ServiceModule } from './modules/service/service.module';
import { DurationInfoServiceModule } from './modules/duration-info-service/duration-info-service.module';
import { QuestionAnswerModule } from './modules/question-answer/question-answer.module';
import { OptionTypeCategorieModule } from './modules/option-type-categorie/option-type-categorie.module';
import { SeoModule } from './modules/seo/seo.module';
import { CommissionModule } from './modules/commission/commission.module';
import { TransactionsModule } from './modules/transactions/transactions.module';
import { ReactiveGuard } from './modules/auth/auth-reactive.guard';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      cache: false,
      //envFilePath: '.local.env',
      envFilePath: process.env.NODE_ENV === 'production' ? '.prod.env' : '.local.env',
      load: [DatabaseConfig],
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => (
      {
        ...configService.get("database"),
      }
      ),
      //dataSourceFactory: async () => ormConfig.initialize(),
    }),
    BullModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => (
        {
          redis: {
            host:configService.get('REDIS_HOST'),
            port:configService.get('REDIS_PORT'),
          },
        }
    
    )}),
    BullModule.registerQueue({
      name: 'message',
      //processors: [join(__dirname, '../../../src/queue/processor.js')],
    }),
    UsersModule,
    AuthModule,
    ChatModule,
    RightsModule,
    MailModule,
    CategoryModule,
    TypeCategorieModule,
    OptionTypeCategorieServiceModule,
    ServiceModule,
    DurationInfoServiceModule,
    QuestionAnswerModule,
    OptionTypeCategorieModule,
    SeoModule,
    CommissionModule,
    TransactionsModule,
  ],
  providers: [ChatEventGateway,ChatConsumer,

    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },

  ],
  exports: [],
})
export class AppModule {
  constructor() {}
}