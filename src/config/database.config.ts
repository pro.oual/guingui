import { registerAs } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';



export default registerAs('database', () => {
  const databaseConfigService = new DatabaseConfigService(new  ConfigService());
  const databaseConfig = databaseConfigService.getDatabaseConfig();
  return {
    type: databaseConfig.type,
    host: databaseConfig.host,
    port: databaseConfig.port,
    username: databaseConfig.username,
    password: databaseConfig.password,
    database: databaseConfig.database,
    entities: [`${__dirname}/../**/*.entity{.ts,.js}`],
    synchronize: false,
    logging: true,
    
    //logging: process.env.NODE_ENV === 'development',
    migrations: [`${__dirname}/../../db/migrations/*{.ts,.js}`],
    migrationsTableName: 'migrations',
  };
});

    

export class DatabaseConfigService {
  constructor(private configService: ConfigService) {}

  getDatabaseConfig() {
    console.log("rr",this.configService.get<string>('DB_TYPE'));

    return {
      type: this.configService.get<string>('DB_TYPE'),
      host: this.configService.get<string>('DB_HOST'),
      port: this.configService.get<number>('DB_PORT'),
      username: this.configService.get<string>('DB_USER'),
      password: this.configService.get<string>('DB_PASSWORD'),
      database: this.configService.get<string>('DB_NAME'),
    };
  }
}