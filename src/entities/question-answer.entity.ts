import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    JoinColumn,
  } from 'typeorm';
import { Service } from './service.entity';
  
  @Entity('QuestionAnswer') // Le nom de la table dans la base de données (sensible à la casse)
  export class QuestionAnswer {




    constructor(partial: Partial<QuestionAnswer>) {
      Object.assign(this, partial);
    }


    
    @PrimaryGeneratedColumn()
    questionAnswer_id: number;
  
    @Column({type:"simple-json", nullable: false })
    question: string[];
  
    @Column({type:"simple-json", nullable: true })
    description_question: string[] | null;

    @Column({type:"simple-json", nullable: false })
    answer: string[] | null;
    
    @Column({type:"simple-json", nullable: true })
    description_answer: string[];
   
  
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;
  
    @ManyToOne(() => Service, { nullable: true })
    @JoinColumn({ name: 'service_id' })
    service: Service;
  }
  