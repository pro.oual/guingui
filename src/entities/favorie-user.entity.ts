import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    OneToMany,
    JoinColumn,
  } from 'typeorm';
import { TypeCategorie } from './type-categorie.entity';
import { Service } from './service.entity';
import { Users } from './user.entity';
  
  @Entity('FavorieUser') // Le nom de la table dans la base de données (sensible à la casse)
  export class FavorieUser {
  
  //pour exclure le mot de passe voir serialization 
  //@Exclude() 

    constructor(partial: Partial<FavorieUser>) {
      Object.assign(this, partial);
    }
    @PrimaryGeneratedColumn()
    favorieUser_id: number;

    @ManyToOne(() => Service, fav => fav.favorie)
    @JoinColumn({name:'service_id'})
    service: number;
   
    @ManyToOne(() => Users, fav => fav.favorie)
    @JoinColumn({name:'user_id'})
    user: number;

    
  }
  