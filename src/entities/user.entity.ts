import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
  Unique,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
} from 'typeorm';
import { UserToRight } from './user_right.entity';
import { Chat } from './chat.entity';
import { Exclude, classToPlain, instanceToPlain } from 'class-transformer';
import { Service } from './service.entity';
import { FavorieUser } from './favorie-user.entity';
import { userRights } from 'src/modules/users/users.admin.service';

@Entity('Users') // Le nom de la table dans la base de données (sensible à la casse)
@Unique(['email', 'siret'])
export class Users {

  
  constructor(partial: Partial<Users>) {
    Object.assign(this, partial);
  }

  @PrimaryGeneratedColumn()
  user_id: number;

  @Column({ type: 'varchar', length: 255, nullable: false })
  first_name: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  last_name: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  username: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  phone_number: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  number_address: string;

  @Column({ type: 'text', nullable: false })
  address: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  zip_code: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  city: string;

  @Column({ type: 'varchar', length: 10, nullable: false })
  genre: string;

  @Column({ type: 'date', nullable: false })
  date_of_birth: Date;

  @Column({ type: 'boolean', default: true })
  is_active: boolean;

  @Column({ type: 'varchar', length: 255, nullable: false })
  email: string;

  @Column({ type: 'boolean', default:false , nullable: true })
  email_verified?: boolean;

  @Column({ type: 'varchar', length: 10, nullable: true })
  local?: string;

  @Column({ type: 'text', nullable: false })
  @Exclude({ toPlainOnly: true })
  password: string;

  @Column({ type: 'text', nullable: true })
  @Exclude({ toPlainOnly: true })
  salt: string;

  @Column({ type: 'datetime', nullable: true })
  last_seen: Date;

  @Column({ type: 'boolean', default: false })
  is_blocked: boolean;

  @Column({ type: 'varchar', length: 255, nullable: true })
  block_reason: string;

  @Column({ type: 'blob', nullable: true })
  avatar: Buffer;

  @Column({ type: 'text', nullable: true })
  token_password_forget: string;

  @Column({ type: 'datetime', nullable: true })
  email_verified_at: Date;

  @Column({ type: 'boolean', default: false, nullable: false })
  general_conditions: boolean;

  
  @Column({ type: 'varchar', length: 255, nullable: false })
  @Exclude({ toPlainOnly: true })
  iban: string;

  @Column({ type: 'varchar', length: 255, nullable: false })
  @Exclude({ toPlainOnly: true })
  bic: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  @Exclude({ toPlainOnly: true })
  bank_name: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  siret: string;

  @Column({ type: 'text', nullable: true })
  facebook: string;
  @Column({ type: 'text', nullable: true })
  instagram: string;
  @Column({ type: 'text', nullable: true })
  pinterest: string;
  @Column({ type: 'text', nullable: true })
  youtube: string;

  @Column({ type: 'boolean', default: true, nullable: true })
  mobilite_entreprise: boolean;

  @Column({ type: 'varchar', nullable: true })
  number_address_mobilite_entreprise: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  address_mobilite_entreprise: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  zip_code_mobilite_entrpeirse: string;

  @Column({ type: 'varchar', length: 255, nullable: true })
  city_mobilite_entrpeirse: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn({ nullable: true })
  updated_at: Date;

  @DeleteDateColumn({ nullable: true })
  deleted_at: Date;

  @Column({ type: 'boolean', default: false, nullable: false })
  is_entreprise: boolean;

  @Column({ type: 'varchar', length: 50, nullable: false })
  role: string;

  @OneToMany(() => UserToRight, (userToRight) => userToRight.user)
  public userToRight: userRights[];

  @OneToMany(() => Chat, chat => chat.sender)
  sentChats: Chat[];

  @OneToMany(() => Chat, chat => chat.receiver)
  receivedChats: Chat[];

  @ManyToMany(() => FavorieUser, fav => fav.user)
  favorie: FavorieUser[];
}
