import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany,
    JoinColumn,
    ManyToOne,
    ManyToMany,
    JoinTable,
  } from 'typeorm';
import { Service } from './service.entity';
import { OptionTypeCategorie } from './option-type-categorie.entity';
  
  @Entity('OptionTypeCategorieService') // Le nom de la table dans la base de données (sensible à la casse)
  export class OptionTypeCategorieService {

    constructor(partial: Partial<OptionTypeCategorieService>) {
      Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn()
    OptionTypeCategorieService_id: number;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    name: string;
  
    @Column({ type: 'text', nullable: false })
    description: string;
  
    
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;


    @ManyToOne(() => Service)
    @JoinColumn({ name: 'service_id' })
    service: Service;
  
    @ManyToOne(() => OptionTypeCategorie, {nullable: true})
    @JoinColumn({ name: 'optionTypeCategorie_id' })
    option: OptionTypeCategorie;

  }
  