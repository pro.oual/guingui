import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, DeleteDateColumn, OneToMany, Transaction, JoinColumn } from 'typeorm';
import { Transactions } from './transactions.entity';

@Entity()
export class Commission {


    
    constructor(partial: Partial<Commission>) {
        Object.assign(this, partial);
      }


  @PrimaryGeneratedColumn()
  commission_id: number;

  @Column({ nullable: true })
  montant: number;

  @Column({ length: 255, nullable: true })
  description: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @DeleteDateColumn()
  deleted_at: Date;

  @Column({ nullable: true })
  amount_min: number;

  @Column({ nullable: true })
  amount_max: number;


  @OneToMany(() => Transactions, transactions => transactions.commission)
  @JoinColumn()
  transactions: Transactions;
}
