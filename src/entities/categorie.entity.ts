import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    OneToMany,
  } from 'typeorm';
import { TypeCategorie } from './type-categorie.entity';
  
  @Entity('Categorie') // Le nom de la table dans la base de données (sensible à la casse)
  export class Categorie {
  
  //pour exclure le mot de passe voir serialization 
  //@Exclude() 
  
    constructor(partial: Partial<Categorie>) {
      Object.assign(this, partial);
    }
    
    @PrimaryGeneratedColumn()
    Categorie_id: number;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    name: string;
  
    @Column({ type: 'text', nullable: false })
    description: string;
  
    
    @CreateDateColumn({ nullable: true })
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;

    @OneToMany(() => TypeCategorie, (typeCategory) => typeCategory.category)
    typeCategories: TypeCategorie[];

    
  }
  