import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany,
    JoinColumn,
    ManyToOne,
    JoinTable,
    ManyToMany,
  } from 'typeorm';
  import { UserToRight } from './user_right.entity';
import { Rights } from './right.entity';
import { Service } from './service.entity';
import { TypeCategorie } from './type-categorie.entity';
import { Exclude } from 'class-transformer';
  
  @Entity('OptionTypeCategorie') // Le nom de la table dans la base de données (sensible à la casse)
  export class OptionTypeCategorie {


          
    constructor(partial: Partial<OptionTypeCategorie>) {
      Object.assign(this, partial);
    }
    

    @PrimaryGeneratedColumn()
    OptionTypeCategorie_id: number;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    name: string;
  
    @Column({ type: 'text', nullable: false })
    description: string;
  
    
    @CreateDateColumn()
    @Exclude()
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  

    //TODO DELETE
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;

    @ManyToOne(() => TypeCategorie, (typeCategory) => typeCategory.options)
    @JoinColumn({ name: 'typeCategorie_id' })
    typeCategory: TypeCategorie;

    @ManyToMany(() => Service)
    @JoinColumn({ name: 'service_id' })
    services: Service[];
  }
  