import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';
import { UserToRight } from './user_right.entity';

@Entity('Rights') // Le nom de la table dans la base de données (sensible à la casse)
export class Rights {
  @PrimaryGeneratedColumn()
  rights_id: number;

  @Column({ type: 'varchar', length: 50, nullable: false })
  name: string;

  @Column({ type: 'text', nullable: false })
  description: string;

  @Column({ type: 'varchar', length: 50, nullable: false })
  domaine: string;

  @Column({ type: 'varchar', length: 50, nullable: false })
  code: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn({ nullable: true })
  updated_at: Date;

  @DeleteDateColumn({ nullable: true })
  deleted_at: Date;

  @OneToMany(() => UserToRight, (userToRight) => userToRight.right)
  public userToRight: UserToRight[];
}
