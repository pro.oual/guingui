import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    JoinColumn,
  } from 'typeorm';
import { Users } from './user.entity';
  @Entity('Chats') 
  export class Chat {
    @PrimaryGeneratedColumn()
    chat_id: number;
  
    @Column({ type: 'text', nullable: false })
    content: string;
    
    @Column({ type: 'varchar', length: 50, nullable: true })
    file_url: string;
  
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;

    @ManyToOne(() => Users, user => user.sentChats)
    @JoinColumn({ name: 'user_sender_id' })
    sender: Users;

    @ManyToOne(() => Users, user => user.receivedChats)
    @JoinColumn({ name: 'user_receiver_id' })
    receiver: Users;
  }
  