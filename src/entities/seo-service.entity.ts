import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToOne,
    JoinColumn,
  } from 'typeorm';
import { Service } from './service.entity';
  
  @Entity('Seo') // Le nom de la table dans la base de données (sensible à la casse)
  export class Seo {



    constructor(partial: Partial<Seo>) {
        Object.assign(this, partial);
      }
    @PrimaryGeneratedColumn()
    seo_id: number;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    meta_name: string;
  
    @Column({ type: 'text', nullable: false })
    meta_description: string;
  
  
    @CreateDateColumn({ nullable: true })
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;
  
    @OneToOne(() => Service, (service) => service.seo)
    @JoinColumn({name:'service_id'})
    public service: Service;
  }
  