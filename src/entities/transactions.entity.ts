import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, Unique, OneToMany } from 'typeorm';
import { Commission } from './commission.entity';
import { Service } from './service.entity';
import { Users } from './user.entity';

@Entity()
export class Transactions {

    constructor(partial: Partial<Transactions>) {
        Object.assign(this, partial);
      }


  @PrimaryGeneratedColumn()
  transaction_id: number;

  @Column({ nullable: true })
  montant: number;

  @Column({ type: 'datetime', nullable: true })
  date_transaction: Date;

  @Column({ length: 10, nullable: true })
  status_transaction: string;

  @Column({ length: 100, nullable: true })
  method_checkout: string;

  @Column({ length: 100, nullable: true })
  reference_paiement: string;

  @Column({ length: 3, nullable: true })
  taxe: string;

  @Column({ length: 1000, nullable: true })
  reject_reason: string;

  @Column({ length: 1000, nullable: true })
  refund_note: string;

  @Column({ length: 10, nullable: true })
  currency: string;

  @Column({ length: 255, nullable: true })
  description: string;

  @Column({ length: 255, nullable: true })

  commission: string;


  @ManyToOne(() => Users)
  @JoinColumn({name:'user_from'})
  user_from: number;


  @ManyToOne(() => Users)
  @JoinColumn({name:'user_to'})
  user_to: number;

  @Column()
  status: string;

  @Column({ length: 50 })
  status_emetteur: string;

  @Column()
  status_destinataire: string;

  @Column()
  status_guingi: string;

  @Column()
  status_prestataire: string;

  @Column('blob')
  file_yousign: Buffer;








  
  @ManyToOne(() => Service, serv => serv.service_request)
  @JoinColumn({name:'service_request_id'})
  service_request: Service;

  @ManyToOne(() => Service, serv => serv.service_sell)
  @JoinColumn({name:'service_sell_id'})
  service_sell: Service;
}
