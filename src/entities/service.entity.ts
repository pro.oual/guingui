import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
    OneToOne,
    Transaction,
  } from 'typeorm';
  import { Users } from './user.entity'; // Assurez-vous d'importer l'entité User ou de définir correctement le chemin d'accès.
import { QuestionAnswer } from './question-answer.entity';
import { DurationInfoService } from './duration-info-service.entity';
import { OptionTypeCategorieService } from './option-type-categorie-service.entity';
import { FavorieUser } from './favorie-user.entity';
import { Seo } from './seo-service.entity';
import { Transactions } from './transactions.entity';
  
  @Entity('Service')
  export class Service {


          
    constructor(partial: Partial<Service>) {
      Object.assign(this, partial);
    }


    @PrimaryGeneratedColumn()
    service_id: number;
  
    @Column({ type: 'int', nullable: true })
    total_amount_service: number;
  
    @Column({ type: 'int', nullable: true })
    already_paid: number;
  
    @Column({ type: 'int', nullable: true })
    amount_want_sell: number;
  
    @Column({ type: 'int', nullable: true })
    new_price: number;
  
    @Column({ type: 'boolean' })
    provider_accepted: boolean;
  
    @Column({ type: 'boolean' })
    have_balance: boolean;
  
    @Column({ type: 'datetime', nullable: true })
    date_validity_balance: Date;
  
    @Column({ type: 'text', nullable: true })
    url_balance: string;
  
    @Column({ type: 'varchar', length: 255 })
    provider_name: string;
  
    @Column({ type: 'varchar', length: 255 })
    email_provider: string;
  
    @Column({ type: 'varchar', length: 20 })
    phone_number: string;
  
    @Column({ type: 'varchar', length: 255 })
    web_site_provider: string;
  
    @Column({ type: 'varchar', length: 255 })
    instagram_provider: string;
  
    @Column({ type: 'varchar', length: 255 })
    facebook_provider: string;
  
    @Column({ type: 'varchar', length: 255 })
    title: string;
  
    @Column({ type: 'text' })
    description: string;
  
    @Column({ type: 'text', nullable: true })
    img1: string;
  
    @Column({ type: 'text', nullable: true })
    img2: string;
  
    @Column({ type: 'text', nullable: true })
    img3: string;
  
    @Column({ type: 'text', nullable: true })
    img4: string;
  
    @Column({ type: 'boolean' })
    valid: boolean;
  
    @Column({ type: 'boolean' })
    valid_back_office: boolean;
  
    @Column({ type: 'varchar', length: 255 })
    name_of_offer: string;
  
    @Column({ type: 'char', length: 1 })
    type_of_service: string;
  
    @Column({ type: 'varchar', length: 255 })
    option_define: string;
  
    
    
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn()
    updated_at: Date;
  
    @DeleteDateColumn()
    deleted_at: Date;
  

    @ManyToOne(() => Users, { nullable: false }) // Assurez-vous que cela correspond à la structure de votre base de données
    @JoinColumn({ name: 'user_id' })
    user: Users;

    @ManyToMany(() => OptionTypeCategorieService)
    options: OptionTypeCategorieService[];
    

    @ManyToMany(() => FavorieUser, fav => fav.service)
    favorie: FavorieUser[];


      
    @OneToOne(() => Seo, (seo) => seo.service)
    public seo: Seo;



    @ManyToMany(() => Transactions, tr => tr.service_request)
    service_request: Transactions;
  
    @ManyToMany(() => Transactions, tr => tr.service_sell)
    service_sell: Transactions;
  }
  