import {
  Entity,
  Column,
  ManyToOne,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm';
import { Users } from './user.entity';
import { Rights } from './right.entity';

@Entity('user_to_right')
export class UserToRight {

  constructor(partial: Partial<UserToRight>) {
    Object.assign(this, partial);
  }

  @PrimaryGeneratedColumn()
  public user_to_right_id: number;

  //ajoute une nouvelle colonne

  @ManyToOne(() => Users, (user) => user.userToRight)
  @JoinColumn({ name: 'user_id' })
  public user: Users;

  @ManyToOne(() => Rights, (right) => right.userToRight)
  @JoinColumn({ name: 'rights_id' })
  public right: Rights;
}
