import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
  } from 'typeorm';
import { Service } from './service.entity';
  
  @Entity('DurationInfoService') // Le nom de la table dans la base de données (sensible à la casse)
  export class DurationInfoService {

    constructor(partial: Partial<DurationInfoService>) {
      Object.assign(this, partial);
    }

    @PrimaryGeneratedColumn()
    DurationInfoService_id: number;
  

    @CreateDateColumn()
    date_of_evenement: Date;

   
    @Column({ type: 'int', nullable: false })
    number_of_hours: number;
  
    @Column({ type: 'int', nullable: false })
    number_of_agent: number;
  

      
    @Column({ type: 'int', nullable: false })
    number_of_guest: number;

    
    @Column({ type: 'varchar', length: 255, nullable: false })
    place_of_service: string;
  
    @CreateDateColumn()
    datetime_of_start

    @CreateDateColumn()
    datetime_of_end

    @Column({ type: 'varchar', length: 255, nullable: false })
    place_of_start
    @Column({ type: 'varchar', length: 255, nullable: false })
    place_of_end
    
    @CreateDateColumn()
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;

    @ManyToOne(() => Service, { nullable: true })
    @JoinColumn({ name: 'service_id' })
    service: Service;
  }
  