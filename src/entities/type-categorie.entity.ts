import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
  } from 'typeorm';
import { Categorie } from './categorie.entity';
import { OptionTypeCategorie } from './option-type-categorie.entity';
  
  @Entity('TypeCategorie') // Le nom de la table dans la base de données (sensible à la casse)
  export class TypeCategorie {


      
    constructor(partial: Partial<TypeCategorie>) {
      Object.assign(this, partial);
    }

    
    @PrimaryGeneratedColumn()
    TypeCategorie_id: number;
  
    @Column({ type: 'varchar', length: 50, nullable: false })
    name: string;
  
    @Column({ type: 'text', nullable: false })
    description: string;
  
    
    @CreateDateColumn({ nullable: true })
    created_at: Date;
  
    @UpdateDateColumn({ nullable: true })
    updated_at: Date;
  
    //TODO DELETE
    @DeleteDateColumn({ nullable: true })
    deleted_at: Date;


    @ManyToOne(() => Categorie, (category) => category.typeCategories)
    @JoinColumn({ name: 'categorie_id' })
    category: Categorie;

    @OneToMany(() => OptionTypeCategorie, (option) => option.typeCategory)
    options: OptionTypeCategorie[];
  }
  