
import { SetMetadata } from '@nestjs/common';
import { Role } from './constants';

export const RIGHTS_KEY = 'rights';
export const Rights = (...roles: Role[]) => SetMetadata(RIGHTS_KEY, roles);