
import { SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_KEY = 'isPublic';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);


export const IS_PUBLIC_ROLE_KEY = 'isPublicRole';
export const PublicRole = () => SetMetadata(IS_PUBLIC_ROLE_KEY, true);

export const IS_PUBLIC_REACTIVE_KEY = 'isPublicReactive';
export const PublicReactive = () => SetMetadata(IS_PUBLIC_REACTIVE_KEY, true);