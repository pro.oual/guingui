import { Test, TestingModule } from '@nestjs/testing';
import { OptionTypeCategorieService } from './option-type-categorie.service';

describe('OptionTypeCategorieService', () => {
  let service: OptionTypeCategorieService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OptionTypeCategorieService],
    }).compile();

    service = module.get<OptionTypeCategorieService>(OptionTypeCategorieService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
