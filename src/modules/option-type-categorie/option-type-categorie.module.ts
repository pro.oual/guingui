import { Module } from '@nestjs/common';
import { OptionTypeCategorieService } from './option-type-categorie.service';
import { OptionTypeCategorieController } from './option-type-categorie.controller';
import { OptionTypeCategorie } from 'src/entities/option-type-categorie.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports:[TypeOrmModule.forFeature([OptionTypeCategorie])],
  controllers: [OptionTypeCategorieController],
  providers: [OptionTypeCategorieService],
})
export class OptionTypeCategorieModule {}
