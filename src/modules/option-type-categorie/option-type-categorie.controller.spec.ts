import { Test, TestingModule } from '@nestjs/testing';
import { OptionTypeCategorieController } from './option-type-categorie.controller';
import { OptionTypeCategorieService } from './option-type-categorie.service';

describe('OptionTypeCategorieController', () => {
  let controller: OptionTypeCategorieController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OptionTypeCategorieController],
      providers: [OptionTypeCategorieService],
    }).compile();

    controller = module.get<OptionTypeCategorieController>(OptionTypeCategorieController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
