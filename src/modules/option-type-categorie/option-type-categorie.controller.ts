import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, HttpStatus, ValidationPipe, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { OptionTypeCategorieService } from './option-type-categorie.service';
import { CreateOptionTypeCategorieDto } from './dto/create-option-type-categorie.dto';
import { UpdateOptionTypeCategorieDto } from './dto/update-option-type-categorie.dto';
import { Public, PublicRole } from 'src/decorators/public.decorator';
import { Role } from 'src/decorators/constants';
import { Roles } from 'src/decorators/roles.decorator';

@Controller('option-type-categorie')

export class OptionTypeCategorieController {
  constructor(private readonly optionTypeCategorieService: OptionTypeCategorieService) {}

  @Roles(Role.Admin)
  @Post()
  create(@Body(new ValidationPipe()) createOptionTypeCategorieDto: CreateOptionTypeCategorieDto) {

    return this.optionTypeCategorieService.create(createOptionTypeCategorieDto);
  }

  @Public()
  @PublicRole()
  @Get()
  findAll() {
    return this.optionTypeCategorieService.findwithRelation();
  }

  @Public()
  @PublicRole()
  @Get(':id')
  findOne(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.optionTypeCategorieService.findOne(+id);
  }

  @Public()
  @PublicRole()
  @Get(':id')
  findOneByTypeCategorie(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.optionTypeCategorieService.findByTypeCategorie(+id);
  }

  @Roles(Role.Admin)
  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateOptionTypeCategorieDto: UpdateOptionTypeCategorieDto) {
    return this.optionTypeCategorieService.update(+id, updateOptionTypeCategorieDto);
  }
  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.optionTypeCategorieService.remove(+id);
  }
}
