import { Transform } from "class-transformer";
import { IsArray, IsDate, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { Service } from "src/entities/service.entity";
import { TypeCategorie } from "src/entities/type-categorie.entity";

export class CreateOptionTypeCategorieDto {

    @IsString()
    @IsNotEmpty()
    name: string;
  
    @IsString()
    @IsNotEmpty()
    description: string;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    
    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    deleted_at: Date;
  
    @IsInt()
    @IsOptional()
    typeCategory: TypeCategorie;
  
    @IsArray()
    @IsOptional()
    services: Service[];


}
