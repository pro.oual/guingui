import { PartialType } from '@nestjs/mapped-types';
import { CreateOptionTypeCategorieDto } from './create-option-type-categorie.dto';

export class UpdateOptionTypeCategorieDto extends PartialType(CreateOptionTypeCategorieDto) {}
