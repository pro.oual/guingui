import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateOptionTypeCategorieDto } from './dto/create-option-type-categorie.dto';
import { UpdateOptionTypeCategorieDto } from './dto/update-option-type-categorie.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { OptionTypeCategorie } from 'src/entities/option-type-categorie.entity';
import { EntityManager, Repository } from 'typeorm';
import { TypeCategorie } from 'src/entities/type-categorie.entity';

@Injectable()
export class OptionTypeCategorieService {
  constructor(
    @InjectRepository(OptionTypeCategorie)
    private optionTypeCategorieRepository: Repository<OptionTypeCategorie>,
    @InjectEntityManager() private readonly entityManager: EntityManager,

  ) {}


  async create(createOptionTypeCategorieDto: CreateOptionTypeCategorieDto) {
    console.log("ide")
    
    const idtc = createOptionTypeCategorieDto.typeCategory as unknown
    console.log("id", createOptionTypeCategorieDto)

    const typeC = await this.entityManager.findOne(TypeCategorie, {
      where:{
        TypeCategorie_id: idtc as number
      }
    });
    console.log(typeC)
    if (!typeC) {
      throw new HttpException('type categorie not found', HttpStatus.NOT_FOUND);
    }
    const opttypeCategorie = this.entityManager.create(OptionTypeCategorie, createOptionTypeCategorieDto);
    opttypeCategorie.typeCategory = typeC;

    return this.entityManager.save(opttypeCategorie)
    
  }

  findAll() {
    
    return this.optionTypeCategorieRepository.find();
  }


  
  async findwithRelation(){

    //la serialization fonctionne correctement de cette façon
    const opts = await this.optionTypeCategorieRepository.createQueryBuilder('otc')
    .leftJoinAndSelect("otc.typeCategory", "otctc")
    .getMany();
    let optsSerialized;
    opts.forEach(element => {
      optsSerialized = new OptionTypeCategorie(element);
    });
    return opts;
  }

  async findByTypeCategorie(id: number){
    const opts = await this.optionTypeCategorieRepository.createQueryBuilder('otc')
    .leftJoinAndSelect("otc.typeCategory", "ctctc")
    .where('ctctc.typeCategory = :typeCategory', {typeCategory: +id})
    .getMany();
    return opts;
  }



  async findOne(id: number) {
    const opts = await this.optionTypeCategorieRepository.createQueryBuilder('otc')
    .leftJoinAndSelect("otc.typeCategory", "otctc")
    .where('otc.OptionTypeCategorie_id = :tc', {tc: +id})
    .getOne();
    
    return opts;
  }

  update(id: number, updateOptionTypeCategoryDto: UpdateOptionTypeCategorieDto) {
    return this.optionTypeCategorieRepository.update(
      +id,
      updateOptionTypeCategoryDto
    );
  }

  remove(id: number) {
    return this.optionTypeCategorieRepository.delete(
      +id
    );
  }
}
