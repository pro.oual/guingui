import { Transform } from "class-transformer";
import { IsString, IsOptional, IsDate, IsNotEmpty } from "class-validator";
import * as moment from "moment";
import { TypeCategorie } from "src/entities/type-categorie.entity";

export class CreateCategoryDto {

    @IsString()
    @IsOptional()
    name: string;
  
    @IsString()
    @IsOptional()
    description: string;
  
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    deleted_at: Date;

    typeCategories: TypeCategorie[];
}
