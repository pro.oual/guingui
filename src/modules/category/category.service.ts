import { Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { IsNull, Repository } from 'typeorm';
import { Categorie } from 'src/entities/categorie.entity';
import moment from 'moment';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Categorie)
    private categorieRepository: Repository<Categorie>
  

  ) {}
  create(createCategoryDto: CreateCategoryDto) {
    return this.categorieRepository.save(createCategoryDto)
  }


  findAll() {
    return this.categorieRepository.find();
  }

  async findOne(id: number) {
    return this.categorieRepository.findBy({
      Categorie_id: +id
    });
  }

  update(id: number, updateCategoryDto: UpdateCategoryDto) {
   
    return this.categorieRepository.update(
      +id,
      updateCategoryDto
    );
  }

  remove(id: number) {
    return this.categorieRepository.delete(
      +id
    );
  }
}
