import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Categorie } from 'src/entities/categorie.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Categorie])],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule {}
