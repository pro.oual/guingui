import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, HttpStatus, ParseIntPipe } from '@nestjs/common';
import { SeoService } from './seo.service';
import { CreateSeoDto } from './dto/create-seo.dto';
import { UpdateSeoDto } from './dto/update-seo.dto';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';

@Controller('seo')
export class SeoController {
  constructor(private readonly seoService: SeoService) {}

  @Roles(Role.Admin, Role.User)
  @Post()
  create(@Body(new ValidationPipe()) createSeoDto: CreateSeoDto) {
    return this.seoService.create(createSeoDto);
  }
  @Roles(Role.Admin, Role.User)

  @Get()
  findAll() {
    return this.seoService.findwithRelation();
  }
  @Roles(Role.Admin, Role.User)

  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.seoService.findOne(+id);
  }
  @Roles(Role.Admin, Role.User)

  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateSeoDto: UpdateSeoDto) {
    return this.seoService.update(+id, updateSeoDto);
  }
  @Roles(Role.Admin, Role.User)

  @Delete(':id')
  remove(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.seoService.remove(+id);
  }
}
