import { Transform } from "class-transformer";
import { IsDate, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { Service } from "src/entities/service.entity";


export class CreateSeoDto { 
    
    @IsString()
    meta_name: string;
  
    @IsString()
    meta_description: string;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    deleted_at: Date;

    service: Service
}
