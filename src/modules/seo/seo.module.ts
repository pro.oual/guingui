import { Module } from '@nestjs/common';
import { SeoService } from './seo.service';
import { SeoController } from './seo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Seo } from 'src/entities/seo-service.entity';


@Module({
  imports:[TypeOrmModule.forFeature([Seo])],
  controllers: [SeoController],
  providers: [SeoService],
})
export class SeoModule {}
