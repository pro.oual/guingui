import { Injectable } from '@nestjs/common';
import { CreateSeoDto } from './dto/create-seo.dto';
import { UpdateSeoDto } from './dto/update-seo.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Seo } from 'src/entities/seo-service.entity';
import { EntityManager, Repository } from 'typeorm';
import { Service } from 'src/entities/service.entity';

@Injectable()
export class SeoService {
  constructor(
    @InjectRepository(Seo)
    private seoRepository: Repository<Seo>,
    @InjectEntityManager() private readonly entityManager: EntityManager,
  ) {}
    //TODO CREE UNE TRANSACTION POUR INSEREZ UN SERVICE QA DURATIONINFOSERVICE ET UNE REQUETE POUR LISTER TOUT LES INFORMATION D'UN SERVICE
  async create(createSeoDto: CreateSeoDto) {
    const serv = await this.entityManager.findOne(Service, {
      where:{
        service_id:createSeoDto.service.service_id
      }
    });
    if (!serv) {
      throw new Error('service not found'); // Gérez le cas où la Category n'est pas trouvée
    }
    const seo = this.entityManager.create(Seo, createSeoDto);
    seo.service = serv;
    const tc = new Seo(seo)
    return this.entityManager.save(tc)
  }



  findAll() {
    return this.seoRepository.find();
  }

  findOne(id: number) {
    return this.seoRepository.findBy({
      seo_id: +id
    });
  }
  
  async findwithRelation(){
    const opts = await this.seoRepository.createQueryBuilder('se')
    .leftJoinAndSelect("se.service", "seosrv")
    .getMany();
    return opts;
  }

  update(id: number, updateSeoDto: UpdateSeoDto) {
    return this.seoRepository.update(
      +id,
      updateSeoDto
    );
  }

  remove(id: number) {
    return this.seoRepository.delete(
      +id
    );
  }
}
