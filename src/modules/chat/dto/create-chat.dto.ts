import { IsInt, IsOptional, IsString } from "class-validator";
import { Users } from "src/entities/user.entity";

export class CreateChatDto {
  @IsString()
  content: string;

  @IsString()
  @IsOptional()
  file_url: string;

  @IsInt()
  sender: Users;

  @IsInt()
  receiver: Users;
}
