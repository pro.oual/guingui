import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, HttpStatus, ParseIntPipe, ClassSerializerInterceptor, UseInterceptors } from '@nestjs/common';
import { ChatService } from './chat.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { User } from 'src/decorators/user.decorator';
import { Users } from 'src/entities/user.entity';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';

@Controller('chat')
@UseInterceptors(ClassSerializerInterceptor)

export class ChatController {
  constructor(private readonly chatService: ChatService) {}
  @Post()
  async create(@Body() createChatDto: CreateChatDto) {
    return await this.chatService.create(createChatDto);
  }

  @Roles(Role.Admin)
  @Get()
  findAll() {
    return this.chatService.findAll();
  }

  @Roles(Role.User)
  @Get('/byuser')
  findOne(@User() user: Users) {
    return this.chatService.findOne(user);
  }


  @Roles(Role.User)
  @Get('/byuser/:id')
  findOneSender(@User() user: Users, @Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.chatService.findOneSender(user, +id);
  }
/*
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateChatDto: UpdateChatDto) {
    return this.chatService.update(+id, updateChatDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.chatService.remove(+id);
  }
*/

}


