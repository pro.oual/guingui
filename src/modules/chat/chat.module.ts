import { Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ChatController } from './chat.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Chat } from 'src/entities/chat.entity';
import { Users } from 'src/entities/user.entity';
import { UsersModule } from '../users/users.module';
import { ChatEventGateway } from '../events/chat-event/chat-event.gateway';

@Module({
  imports:[TypeOrmModule.forFeature([Chat,Users]), UsersModule],
  controllers: [ChatController],
  providers: [ChatService],
  exports:[ChatService]
})
export class ChatModule {}
