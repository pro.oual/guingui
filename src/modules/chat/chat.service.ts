import { Injectable } from '@nestjs/common';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Chat } from 'src/entities/chat.entity';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { Users } from 'src/entities/user.entity';

@Injectable()
export class ChatService {

    constructor(
      @InjectRepository(Chat)
      private readonly chatRepository: Repository<Chat>) 
      {}

        async create(createChatDto: CreateChatDto) {
          return await this.chatRepository.save(createChatDto);
        }

        async findAll() {
          return await this.chatRepository.find();
        }

        async findOne(user: Users) {
        try{
          const userChat = await this.chatRepository.createQueryBuilder('chat')
          .leftJoinAndSelect("chat.sender", "chatSender")
          .leftJoinAndSelect("chat.receiver", "chatRender")
          .where('chat.sender = :userId', { userId: user.user_id })
          .orWhere('chat.receiver = :userId',{ userId: user.user_id})
          .getMany(); 

  
          return userChat;
        }catch(err){
          console.log(err)
        }
      }


      async findOneSender(user: Users, id: number) {
        try{
          const userChat = await this.chatRepository.createQueryBuilder('chat')
          .leftJoinAndSelect("chat.sender", "chatSender")
          .leftJoinAndSelect("chat.receiver", "chatRender")
          .where('chat.sender = :userIds', { userIds: user.user_id })
          .andWhere('chat.receiver = :userIdr',{ userIdr: id})
          .getMany(); 
          return userChat;
        }catch(err){
          console.log(err)
        }
      }
      /*
        update(id: number, updateChatDto: UpdateChatDto) {
          return `This action updates a #${id} chat`;
        }

        remove(id: number) {
          return `This action removes a #${id} chat`;
        }
      */
  }
