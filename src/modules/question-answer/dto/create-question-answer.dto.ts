import { Transform } from "class-transformer";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { Service } from "src/entities/service.entity";

export class CreateQuestionAnswerDto {
    
    @IsString()
    @IsNotEmpty()
    question: string[]| null;
    
    @IsString()
    @IsOptional()
    description_question: string[] | null;

    @IsString()
    @IsNotEmpty()
    answer: string[]| null;
    
    @IsString()
    @IsOptional()
    description_answer: string[] | null;
 
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    created_at: Date;
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    updated_at: Date | null;
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    deleted_at: Date | null;

    @IsNotEmpty()
    service: Service;
}
