import { Module } from '@nestjs/common';
import { QuestionAnswerService } from './question-answer.service';
import { QuestionAnswerController } from './question-answer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuestionAnswer } from 'src/entities/question-answer.entity';

@Module({
  imports:[TypeOrmModule.forFeature([QuestionAnswer])],
  controllers: [QuestionAnswerController],
  providers: [QuestionAnswerService],
})
export class QuestionAnswerModule {}
