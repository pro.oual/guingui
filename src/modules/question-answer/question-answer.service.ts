import { Injectable } from '@nestjs/common';
import { CreateQuestionAnswerDto } from './dto/create-question-answer.dto';
import { UpdateQuestionAnswerDto } from './dto/update-question-answer.dto';
import { QuestionAnswer } from 'src/entities/question-answer.entity';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { Service } from 'src/entities/service.entity';

@Injectable()
export class QuestionAnswerService {
  
  
  constructor(
    @InjectRepository(QuestionAnswer)
    private questionAnswerRepository: Repository<QuestionAnswer>,
    @InjectEntityManager() private readonly entityManager: EntityManager,

  
  ) {}
  async create(createQuestionAnswerDto: CreateQuestionAnswerDto) {

    const service = await this.entityManager.findOne(Service, {
      where:{
        service_id:createQuestionAnswerDto.service.service_id
      }
    });
    if (!service) {
      throw new Error('service not found'); // Gérez le cas où la Category n'est pas trouvée
    }
    const qaplain = this.entityManager.create(QuestionAnswer, createQuestionAnswerDto);
    qaplain.service = service;
    const qa = new QuestionAnswer(qaplain)
    return this.entityManager.save(qa)
  }

  findAll() {
    return this.questionAnswerRepository.find();
  }

  async findwithRelation(){
    const opts = await this.questionAnswerRepository.createQueryBuilder('qa')
    .leftJoinAndSelect("qa.service", "qasrv")
    //.where('chat.sender = :userId', { userId: user.user_id })
    //.orWhere('chat.receiver = :userId',{ userId: user.user_id})
    .getMany();
    return opts;
  }

  findOne(id: number) {
    return this.questionAnswerRepository.findBy({
      questionAnswer_id: +id
    });
  }

  update(id: number, updateQuestionAnswerDto: UpdateQuestionAnswerDto) {
    return this.questionAnswerRepository.update(
      +id,
      updateQuestionAnswerDto
    );
  }

  remove(id: number) {
    return this.questionAnswerRepository.delete(
      +id
    );
  }
 
}
