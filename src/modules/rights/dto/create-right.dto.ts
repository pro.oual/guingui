import { IsDate, IsOptional, IsString } from 'class-validator';

export class CreateRightDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsOptional()
  code: string;

  @IsString()
  @IsOptional()
  domaine: string;

  @IsDate()
  created_at: Date;

  @IsDate()
  @IsOptional()
  updated_at: Date;

  @IsDate()
  @IsOptional()
  deleted_at: Date;
}
