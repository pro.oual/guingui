import { Module } from '@nestjs/common';
import { RightsService } from './rights.service';
import { RightsController } from './rights.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rights } from 'src/entities/right.entity';

@Module({
  controllers: [RightsController],
  providers: [RightsService],
  exports: [TypeOrmModule],
  imports: [TypeOrmModule.forFeature([Rights])],
})
export class RightsModule {}
