import { Injectable } from '@nestjs/common';
import { CreateRightDto } from './dto/create-right.dto';
import { UpdateRightDto } from './dto/update-right.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Rights } from 'src/entities/right.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RightsService {
  constructor(
    @InjectRepository(Rights)
    private rightRepository: Repository<Rights>,
  ) {}

  async create(createRightDto: CreateRightDto) {
    return this.rightRepository.save(createRightDto);
  }

  async findAll() {
    return this.rightRepository.find();
  }

  async findOne(id: number) {
    return this.rightRepository.findOneBy({
      rights_id: +id,
    });
  }

  async update(id: number, updateRightDto: UpdateRightDto) {
    return this.rightRepository.update(+id, updateRightDto);
  }

  async remove(id: number) {
    return this.rightRepository.delete({
      rights_id: +id,
    });
  }
}
