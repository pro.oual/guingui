import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RightsService } from './rights.service';
import { CreateRightDto } from './dto/create-right.dto';
import { UpdateRightDto } from './dto/update-right.dto';
import { Role } from 'src/decorators/constants';
import { Roles } from 'src/decorators/roles.decorator';

@Controller('rights')
export class RightsController {
  constructor(private readonly rightsService: RightsService) {}

  @Roles(Role.Admin)
  @Post()
  create(@Body() createRightDto: CreateRightDto) {
    return this.rightsService.create(createRightDto);
  }

  @Roles(Role.Admin)
  @Get()
  findAll() {
    return this.rightsService.findAll();
  }

  @Roles(Role.Admin)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.rightsService.findOne(+id);
  }

  @Roles(Role.Admin)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateRightDto: UpdateRightDto) {
    return this.rightsService.update(+id, updateRightDto);
  }
  
  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.rightsService.remove(+id);
  }
}
