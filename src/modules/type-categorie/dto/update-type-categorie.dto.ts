import { PartialType } from '@nestjs/mapped-types';
import { CreateTypeCategorieDto } from './create-type-categorie.dto';

export class UpdateTypeCategorieDto extends PartialType(CreateTypeCategorieDto) {}
