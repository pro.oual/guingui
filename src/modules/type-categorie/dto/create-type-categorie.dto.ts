import { Transform } from "class-transformer";
import { IsString, IsOptional, IsDate } from "class-validator";
import * as moment from "moment";
import { Categorie } from "src/entities/categorie.entity";

export class CreateTypeCategorieDto {
    @IsString()
    @IsOptional()
    name: string;
  
    @IsString()
    @IsOptional()
    description: string;
  
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()

    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    @IsOptional()
    deleted_at: Date;
 
    category: Categorie;
}

