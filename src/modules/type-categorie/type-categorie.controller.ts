import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, ParseIntPipe, HttpStatus } from '@nestjs/common';
import { TypeCategorieService } from './type-categorie.service';
import { CreateTypeCategorieDto } from './dto/create-type-categorie.dto';
import { UpdateTypeCategorieDto } from './dto/update-type-categorie.dto';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';
import { Public, PublicRole } from 'src/decorators/public.decorator';

@Controller('type-categorie')
export class TypeCategorieController {
  constructor(private readonly typeCategorieService: TypeCategorieService) {}

  @Roles(Role.Admin)
  @Post()
  create(@Body(new ValidationPipe()) createTypeCategorieDto: CreateTypeCategorieDto) {
    return this.typeCategorieService.create(createTypeCategorieDto);
  }
  @Public()
  @PublicRole()
  @Get()
  findAll() {
    return this.typeCategorieService.findwithRelation();
  }
  
  @Public()
  @PublicRole()
  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.typeCategorieService.findOne(+id);
  }

  @Public()
  @PublicRole()
  @Get('/categorie/:id')
  findByCategorie(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.typeCategorieService.findByCategorie(+id);
  }

  @Roles(Role.Admin)
  @Patch(':id')
  update(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateTypeCategorieDto: UpdateTypeCategorieDto) {
    return this.typeCategorieService.update(+id, updateTypeCategorieDto);
  }

  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.typeCategorieService.remove(+id);
  }
}
