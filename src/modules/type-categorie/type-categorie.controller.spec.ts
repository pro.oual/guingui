import { Test, TestingModule } from '@nestjs/testing';
import { TypeCategorieController } from './type-categorie.controller';
import { TypeCategorieService } from './type-categorie.service';

describe('TypeCategorieController', () => {
  let controller: TypeCategorieController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypeCategorieController],
      providers: [TypeCategorieService],
    }).compile();

    controller = module.get<TypeCategorieController>(TypeCategorieController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
