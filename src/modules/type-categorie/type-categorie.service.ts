import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateTypeCategorieDto } from './dto/create-type-categorie.dto';
import { UpdateTypeCategorieDto } from './dto/update-type-categorie.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { EntityManager, Repository } from 'typeorm';
import { TypeCategorie } from 'src/entities/type-categorie.entity';
import { Categorie } from 'src/entities/categorie.entity';

@Injectable()
export class TypeCategorieService {
  
  constructor(
    @InjectRepository(TypeCategorie)
    private typeCategorieRepository: Repository<TypeCategorie>,
    @InjectEntityManager() private readonly entityManager: EntityManager,
  ) {}


  async create(createTypeCategorieDto: CreateTypeCategorieDto) {
    const idc = createTypeCategorieDto.category as unknown
    const category = await this.entityManager.findOne(Categorie, {
      where:{
        Categorie_id:idc as number
      }
    });
    if (!category) {
      throw new HttpException('Category not found', HttpStatus.NOT_FOUND); // Gérez le cas où la Category n'est pas trouvée
    }
    const typeCategorie = this.entityManager.create(TypeCategorie, createTypeCategorieDto);
    typeCategorie.category = category;
    const tc = new TypeCategorie(typeCategorie)
    return this.entityManager.save(tc)
  }

  findAll() {
    return this.typeCategorieRepository.find();
  }
  async findwithRelation(){
    const opts = await this.typeCategorieRepository.createQueryBuilder('tc')
    .leftJoinAndSelect("tc.category", "tcc")
    .getMany();
    return opts;
  }

  async findByCategorie(id: number){
    const opts = await this.typeCategorieRepository.createQueryBuilder('tc')
    .leftJoinAndSelect("tc.category", "tcc")
    .where('tc.category = :category', {category: +id})
    .getMany();
    return opts;
  }
  
  findOne(id: number) {
    return this.typeCategorieRepository.findBy({
      TypeCategorie_id: +id
    });
  }

  update(id: number, updateTypeCategoryDto: UpdateTypeCategorieDto) {
    return this.typeCategorieRepository.update(
      +id,
      updateTypeCategoryDto
    );
  }

  remove(id: number) {
    return this.typeCategorieRepository.delete(
      +id
    );
  }
}
