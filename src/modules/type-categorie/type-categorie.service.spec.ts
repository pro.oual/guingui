import { Test, TestingModule } from '@nestjs/testing';
import { TypeCategorieService } from './type-categorie.service';

describe('TypeCategorieService', () => {
  let service: TypeCategorieService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypeCategorieService],
    }).compile();

    service = module.get<TypeCategorieService>(TypeCategorieService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
