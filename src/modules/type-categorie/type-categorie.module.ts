import { Module } from '@nestjs/common';
import { TypeCategorieService } from './type-categorie.service';
import { TypeCategorieController } from './type-categorie.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeCategorie } from 'src/entities/type-categorie.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TypeCategorie])],
  controllers: [TypeCategorieController],
  providers: [TypeCategorieService],
})
export class TypeCategorieModule {}
