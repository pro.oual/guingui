import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Users } from '../../entities/user.entity';
@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

  async sendUserConfirmation(user: Users, token: string) {
    //TODO : URL A DEFINIRE
    const url = `http://localhost/test_front/index.html?token=${token}`;

    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Welcome to Nice App! Confirm your Email',
      template: './confirmation', // `.hbs` extension is appended automatically
      context: { 
        name: user.last_name,
        url,
      },
    });
  }
  async sendUserConfirmationRegistration(user: Users, token: string) {
    //TODO : URL A DEFINIRE

    const url = `http://localhost/test_front/modify-password&token=${token}`;

    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Reset password',
      template: './resetPassword', // `.hbs` extension is appended automatically
      context: { 
        name: user.last_name,
        url,
      },
    });
  }

  async sendUserConfirmationPasswordChange(user: Users) {
    //TODO : URL A DEFINIRE

   
    await this.mailerService.sendMail({
      to: user.email,
      subject: 'Reset password',
      template: './passwordChange', // `.hbs` extension is appended automatically
      context: { 
        name: user.last_name,
      },
    });
  }
}
