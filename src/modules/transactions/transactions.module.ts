import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { Transactions } from 'src/entities/transactions.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from 'src/entities/service.entity';
import { Users } from 'src/entities/user.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Transactions, Service, Users])],
  controllers: [TransactionsController],
  providers: [TransactionsService],
})
export class TransactionsModule {}
