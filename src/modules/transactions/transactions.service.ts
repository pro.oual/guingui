import { HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { InjectRepository, InjectEntityManager } from '@nestjs/typeorm';
import { DurationInfoService } from 'src/entities/duration-info-service.entity';
import { QuestionAnswer } from 'src/entities/question-answer.entity';
import { Service } from 'src/entities/service.entity';
import { Repository, EntityManager, DataSource } from 'typeorm';
import { OptionTypeCategorieService } from '../option-type-categorie/option-type-categorie.service';
import { Transactions } from 'src/entities/transactions.entity';
import { CreateTransactionInfoDto } from './dto/create-transaction-info.dto';
import { CreateStatusDestinataireDto } from './dto/create-status-destinataire.dto';
import { CreateStatusGuingiDto } from './dto/create-status-guingi.dto';
import { CreateStatusPrestataireDto } from './dto/create-status-prestataire.dto';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
    @InjectRepository(Transactions)
    private transactionsRepository: Repository<Transactions>,
    @InjectEntityManager() private readonly entityManager: EntityManager,
    private dataSource: DataSource,
  ) {}


  async create(
    createTransactionInfoDto: CreateTransactionInfoDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      const transactions = new Transactions(createTransactionInfoDto)
      transactions.status_emetteur = "valider"
      await queryRunner.manager.save(transactions);
      await queryRunner.commitTransaction();
      return {
        status: HttpStatus.CREATED,
        message: 'succes',
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new InternalServerErrorException("Une erreur est survenue."+err);
    } finally {
      await queryRunner.release();
    }
  }


  async createStatusDestinataire(
    id: number,
    createStatusDestinataireDto: CreateStatusDestinataireDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      //TODO : faire une verification sur le montant reçu
      //verifier le paiement
      await this.update(+id,createStatusDestinataireDto)
      await queryRunner.commitTransaction();
      return {
        status: HttpStatus.CREATED,
        message: 'succes',
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new InternalServerErrorException("Une erreur est survenue."+err);
    } finally {
      await queryRunner.release();
    }
  }

  async createStatusGuingi(
    id: number,
    createStatusGuingiDto: CreateStatusGuingiDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await this.update(+id,createStatusGuingiDto)
      await queryRunner.commitTransaction();
      return {
        status: HttpStatus.CREATED,
        message: 'succes',
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new InternalServerErrorException("Une erreur est survenue."+err);
    } finally {
      await queryRunner.release();
    }
  }


  async createStatusPrestataire(
    id: number,
    createStatusprestataireDto: CreateStatusPrestataireDto) {
    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await this.update(+id,createStatusprestataireDto)
      await queryRunner.commitTransaction();
      return {
        status: HttpStatus.CREATED,
        message: 'succes',
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new InternalServerErrorException("Une erreur est survenue."+err);
    } finally {
      await queryRunner.release();
    }
  }



  findAll() {
    return this.transactionsRepository.find();
  }

  findOne(id: number) {
    return this.transactionsRepository.findBy({
      transaction_id: +id
    });
  }

  update(id: number, updateTransactionDto: UpdateTransactionDto) {
    return this.transactionsRepository.update(
      +id,
      updateTransactionDto
    );
  }

  remove(id: number) {
    return `This action removes a #${id} transaction`;
  }
}
