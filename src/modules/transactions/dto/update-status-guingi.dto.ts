import { PartialType } from "@nestjs/mapped-types";
import { CreateStatusGuingiDto } from "./create-status-guingi.dto";

export class UpdateStatusGuingiDto extends PartialType(CreateStatusGuingiDto) {}
