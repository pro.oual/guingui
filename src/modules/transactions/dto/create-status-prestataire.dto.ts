import { IsString } from "class-validator";

export class CreateStatusPrestataireDto {
  
    @IsString()
    status_prestataire: string;

}
