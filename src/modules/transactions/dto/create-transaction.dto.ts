import { IsNumber, IsOptional, IsDate, IsString } from "class-validator";

export class CreateTransactionDto {
  
    @IsNumber()
    @IsOptional()
    montant: number;
  
    @IsDate()
    @IsOptional()
    date_transaction: Date;
  
    @IsString()
    @IsOptional()
    status_transaction: string;
  
    @IsString()
    @IsOptional()
    method_checkout: string;
  
    @IsString()
    @IsOptional()
    reference_paiement: string;
  
    @IsString()
    @IsOptional()
    taxe: string;
  
    @IsString()
    @IsOptional()
    reject_reason: string;
  
    @IsString()
    @IsOptional()
    refund_note: string;
  
    @IsString()
    @IsOptional()
    currency: string;
  
    @IsString()
    @IsOptional()
    description: string;
  
    @IsString()
    @IsOptional()
    commission: string;
  
    @IsNumber()
    @IsOptional()
    user_from: number;
  
    @IsNumber()
    @IsOptional()
    user_to: number;
  

    @IsString()
    status: string;
  
    @IsString()
    status_emetteur: string;
  
    @IsString()
    status_destinataire: string;
  
    @IsString()
    status_guingi: string;
  
    @IsString()
    status_prestataire: string;
  
    
    file_yousign: Buffer;
  
    @IsNumber()
    @IsOptional()
    service_request_id: number;
  
    @IsNumber()
    @IsOptional()
    service_sell_id: number;
}
