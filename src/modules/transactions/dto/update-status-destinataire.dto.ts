import { IsString } from "class-validator";
import { CreateStatusDestinataireDto } from "./create-status-destinataire.dto";
import { PartialType } from "@nestjs/mapped-types";

export class UpdateStatusDestinataireDto extends PartialType(CreateStatusDestinataireDto) {}
