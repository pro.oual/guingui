import { Type } from "class-transformer";
import { IsNumber, IsOptional, IsDate, IsString } from "class-validator";
import { Service } from "src/entities/service.entity";

export class CreateTransactionInfoDto {
    

    
    @IsNumber()
    @IsOptional()
    user_from: number;
    
    @IsNumber()
    @IsOptional()
    user_to: number;
    
    
    @Type(() => Service) 
    service_request_id: number;

    
    @Type(() => Service) 
    service_sell_id: number;
}
