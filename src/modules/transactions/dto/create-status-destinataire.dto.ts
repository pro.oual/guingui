import { IsString } from "class-validator";

export class CreateStatusDestinataireDto {
  
    @IsString()
    status_destinataire: string;

}
