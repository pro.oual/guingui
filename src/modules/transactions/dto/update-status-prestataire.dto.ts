import { PartialType } from "@nestjs/mapped-types/dist";
import { CreateStatusPrestataireDto } from "./create-status-prestataire.dto";

export class UpdateStatusPrestataireDto extends PartialType(CreateStatusPrestataireDto) {}