import { PartialType } from "@nestjs/mapped-types";
import { CreateTransactionInfoDto } from "./create-transaction-info.dto";

export class UpdateTransactionInfoDto extends PartialType(CreateTransactionInfoDto) {}
