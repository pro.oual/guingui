import { IsString } from "class-validator";

export class CreateStatusGuingiDto {
  
    @IsString()
    status_guingi: string;

}
