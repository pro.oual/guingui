import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, HttpStatus } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { Public, PublicRole } from 'src/decorators/public.decorator';
import { CreateStatusDestinataireDto } from './dto/create-status-destinataire.dto';
import { CreateStatusGuingiDto } from './dto/create-status-guingi.dto';
import { CreateStatusPrestataireDto } from './dto/create-status-prestataire.dto';
@Public()
@PublicRole()
@Controller('transactions')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @Post()
  create(@Body() createTransactionDto: CreateTransactionDto) {
    return this.transactionsService.create(createTransactionDto);
  }


  @Patch('/:id/status-destinataire')
  createStatusDestinataire(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,@Body() createStatusDestinataireDto: CreateStatusDestinataireDto) {
    return this.transactionsService.createStatusDestinataire(+id,createStatusDestinataireDto);
  }


  @Patch('/:id/status-guingi')
  createStatusGuingi(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,@Body() createStatusGuingiDto: CreateStatusGuingiDto) {
    return this.transactionsService.createStatusGuingi(+id,createStatusGuingiDto);
  }


  @Patch('/:id/status-prestataire')
  createStatusPrestataire(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,@Body() createStatusPrestataireDto: CreateStatusPrestataireDto) {
    return this.transactionsService.createStatusPrestataire(+id,createStatusPrestataireDto);
  }


  @Get()
  findAll() {
    return this.transactionsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.transactionsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTransactionDto: UpdateTransactionDto) {
    return this.transactionsService.update(+id, updateTransactionDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.transactionsService.remove(+id);
  }
}
