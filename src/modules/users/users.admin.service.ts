import { HttpException, HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository, DataSource, IsNull } from 'typeorm';
import { Users } from 'src/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserToRight } from 'src/entities/user_right.entity';
import { Rights } from 'src/entities/right.entity';
import { CreateUserAdminDto } from './dto/create-user-admin.dto';
import * as bcrypt from 'bcrypt';
import { Role } from 'src/decorators/constants';
import * as moment from 'moment';

@Injectable()
export class UsersAdminService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
    @InjectRepository(UserToRight)
    private usersToRightsRepository: Repository<UserToRight>,
    private dataSource: DataSource,
  ) {}

  async findAllAdmin() {
    return this.usersRepository.find({
      where: {
        role: Role.Admin
      },
    });
  }


  async findAllAdminWithDeleted() {
    return await this.usersRepository.createQueryBuilder('u')
    .where('u.role = :role' , {role: Role.Admin})
    .withDeleted()
    .getMany();
  }

  async findOneAdmin(id: number) {
    const userAdmin = await this.usersRepository.findOneBy({
      user_id: +id,
      role: Role.Admin,
    });
    if(!userAdmin){
      throw new HttpException("Utilisateur inexistant", HttpStatus.NOT_FOUND)
    }
    return new Users(userAdmin)
  }

  async updateAdmin(id: number, updateUserDto: UpdateUserDto) {
    const userAdmin = this.findOneAdmin(+id)
    if(userAdmin){
      updateUserDto.created_at =  moment(moment.now()).toDate()
      const updUser =  this.usersRepository.create(updateUserDto)
      return  this.usersRepository.update(+id, updUser);
    }
    throw new HttpException("Impssible de modifier cet utilisateur", HttpStatus.NOT_FOUND)

  }

  async removeAdmin(id: number) {
    const userAdmin = await this.findOneAdmin(+id)
    console.log(userAdmin)
    if(userAdmin){
      return  this.usersRepository.update(
        +id,
       {
        deleted_at: moment(moment.now()).toDate()
       }
      );
    }
    throw new HttpException("Utilisateur déja supprimer", HttpStatus.NOT_FOUND)
  }

  async userRights(id: number): Promise<UserToRight[]> {
    const userAdmin = await this.findOneAdmin(+id)
    if(userAdmin){
      const userRights = await this.usersToRightsRepository.find({
            where: {
              user: {
                user_id: +id,
              },
            },
          });
      return userRights;
    }
    throw new HttpException("Utilisateur avec aucun droit", HttpStatus.NOT_FOUND)
    
  }

  async deleteUserRights(id: number, rights: UserToRight[]) {
    //TODO CREE UNE REQUETE
    const user = await this.findOneAdmin(+id);

    if(user){
      rights.forEach(async (element) => {
        await this.usersToRightsRepository.delete({
          user: user,
          user_to_right_id: element.user_to_right_id,
        });
      });

      return {
        status: HttpStatus.NO_CONTENT,
        message: 'Ce(s) droit(s) à etait retire avec succes',
      };
    }
    throw new HttpException("Impossible de retire ce droit", HttpStatus.NOT_FOUND)
  }

  async createAdmin(CreateUserAdminDto: CreateUserAdminDto) {   
      const salt = await bcrypt.genSalt();
      const queryRunner = this.dataSource.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
      try {
        const newUser = new Users(CreateUserAdminDto);
        newUser.password = await bcrypt.hash(CreateUserAdminDto.password, salt);
        newUser.salt = salt;
        newUser.role = Role.Admin;
        newUser.created_at =  moment(moment.now()).toDate()
        await queryRunner.manager.save(newUser);
        CreateUserAdminDto.userToRight.forEach(async (element, index) => {
        const right = await queryRunner.manager.findOneBy(Rights, {
          rights_id: element.rights_id
        });
        const userRight = new UserToRight({
          user:newUser,
          right: right
        })
        await queryRunner.manager.save(userRight);
        }
        );
        await queryRunner.commitTransaction();
        return {
          status: HttpStatus.CREATED,
          message: 'Admin cree avec succes',
        };
      } catch (err) {
        await queryRunner.rollbackTransaction();
        throw new InternalServerErrorException("Une erreur est survenue.");
      } finally {
        await queryRunner.release();
      }
  }

  async findOneAdminByEmail(email: string) {
    const userAdmin = await this.usersRepository.findOneBy({
      email: email,
      role: Role.Admin,
    });

    if(!userAdmin){
      return userAdmin;
    }
    return new Users(userAdmin);
  }
}

export interface userRights{
  user_id?: number,
  rights_id: number 
}