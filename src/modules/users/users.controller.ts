import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  ValidationPipe,
  ParseIntPipe,
  HttpStatus,
  Req,
  ClassSerializerInterceptor,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { SearchUserByEmailDto } from './dto/get-email.dto';
import { Public, PublicReactive, PublicRole } from 'src/decorators/public.decorator';
import { DuplicateEmailException } from './DuplicateEmailException';
import { DuplicateSiretException } from './DuplicateSiretException';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';
import { Users } from 'src/entities/user.entity';
import { CreateUserFavDto } from './dto/create-user-fav.dto';
import Stripe from 'stripe';
import { ConfigService } from '@nestjs/config';
import { DeleteUserFavDto } from './dto/delete-user-fav.dto';
import { User } from 'src/decorators/user.decorator';
import { ResponsiveUserDto } from './dto/responsive-user.dto';


@Controller('users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {

  private stripe: Stripe;
  constructor(private readonly usersService: UsersService, private readonly configService: ConfigService) {

    this.stripe = new Stripe('sk_test_51NgVRIAyWtQXAtnBGGWMRBRs3PaKPfn8o1W0inMbcb5j6NN0A72Z5rNZBlUrKS1d6cO7xTaZ6ghSnQ2gJJCIUcEd003TeeLtXy', {
      apiVersion: '2023-10-16', // Assurez-vous d'utiliser une version compatible de l'API Stripe
    });
  }

  @Public()
  @PublicRole()
  @Post()
  async create(@Body(new ValidationPipe()) createUserDto: CreateUserDto) {
    const emailAlreadyExist = await this.usersService.findOneByEmailForSignIn(createUserDto.email)
    if(emailAlreadyExist){
      throw new DuplicateEmailException;
    }
   /* const siretAlreadyExist = await this.usersService.findOneBySiret(createUserDto.siret)
    if(siretAlreadyExist && createUserDto.is_entreprise === true){
      throw new DuplicateSiretException;
    }*/
    return await this.usersService.create(createUserDto);
  }

  @Roles(Role.User, Role.Admin)
  @Post('/add/wishlist')
  async favories(@Body(new ValidationPipe()) createUserFavDto: CreateUserFavDto, @User() userConnected: any) {
    return await this.usersService.makeFav(createUserFavDto, userConnected);
  }

  @Roles(Role.User, Role.Admin)
  @Post('/delete/wishlist')
  async deleteFavorie(@Body(new ValidationPipe()) deleteUserFavDto: DeleteUserFavDto, @User() userConnected: any) {
    return await this.usersService.deleteFav(deleteUserFavDto,userConnected);
  }

  @Roles(Role.Admin)
  @Post('/search')
  async findUserByEmail(@Body(new ValidationPipe()) searchUserByEmailDto: SearchUserByEmailDto) {
    const user = await this.usersService.findOneByEmail(
      searchUserByEmailDto.email,
    );
    return user;
  }

  @Roles(Role.Admin)
  @Get()
  async findAll() {
    return await this.usersService.findAll();
  }

  @Roles(Role.Admin)
  @Get('/with-deleted')
  async findAllWithDelete() {
    return await this.usersService.findAllWithDeleted();
  }

  @Roles(Role.Admin, Role.User)
  @Get('/:id/reactive')
  async responsiveUser(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, 
  @Req() req: any) {
    return await this.usersService.responsiveUser(+id,req.user);
  }


  @Roles(Role.Admin,Role.User)
  @Get(':id')
  async findOne(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.usersService.findOne(+id);
  }



 /*@Get('/profile')
  async findOneTest(@User('username') firstName: string) {
    console.log(firstName);
  }*/


  @Roles(Role.Admin, Role.User)
  @Patch(':id')
  async update(@Param('id',  new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateUserDto: UpdateUserDto,@Req() req: any) {
    return await this.usersService.update(+id, updateUserDto, req.user);
  }
  
  @Roles(Role.Admin, Role.User)
  @Delete(':id')
  async remove(@Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Req() req: any) {
    return await this.usersService.remove(+id, req.user);
  }

/* EXPERIMENTALE */
  @Roles(Role.Admin, Role.User)
  @Post('create-seller')
  async createSellerAccount(@Body() data: { email: string }) {
    const account = await this.usersService.createSellerAccount(data.email);   
    return account;
  }

  @Roles(Role.Admin, Role.User)
  @Post('add-credit-card')
  async addCreditCardToSeller(@Body() data: { accountId: string, cardToken: string }) {
    const card = await this.usersService.addCreditCardToSeller(data.accountId, data.cardToken);
    return card;
  }

  @Roles(Role.Admin, Role.User)
  @Post('create-payment-intent')
  async createPaymentIntent(@Body() data: { amount: number, customerId: string, accountId: string }) {
    const paymentIntent = await this.usersService.createPaymentIntent(data.amount, data.customerId, data.accountId);
    return paymentIntent;
  }

  
}
