import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  Req,
  ValidationPipe,
  ParseIntPipe,
  HttpStatus,
  UseInterceptors,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersAdminService } from './users.admin.service';
import { UpdateUserAdminDto } from './dto/update-user-admin.dto';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';
import { DuplicateEmailException } from './DuplicateEmailException';
import { DuplicateSiretException } from './DuplicateSiretException';
import { SearchUserByEmailDto } from './dto/get-email.dto';
import { UserToRight } from 'src/entities/user_right.entity';
@UseInterceptors(ClassSerializerInterceptor)
@Controller('admin')
export class AdminController {
  constructor(private readonly usersAdminService: UsersAdminService) {}


  @Roles(Role.Admin)
  @Post()
  async create(@Body(new ValidationPipe()) CreateUserAdminDto) {
    const emailAlreadyExist = await this.usersAdminService.findOneAdminByEmail(CreateUserAdminDto.email)
    if(emailAlreadyExist){
      throw new DuplicateEmailException;
    }
    return await this.usersAdminService.createAdmin(CreateUserAdminDto);
  }



  //LISTER TOUS LES RIGHTS D'UN ADMIN
  @Roles(Role.Admin)
  @Get('/:id/rights')
  async findUserWithRight(@Param('id',  new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.usersAdminService.userRights(+id);
  }
  

  @Roles(Role.Admin)
  @Delete('/:id/rights')
  async deleteRightForUser(
    @Param('id',  new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,
    @Body(new ValidationPipe()) userRights: UserToRight[]) {
    return await this.usersAdminService.deleteUserRights(+id,userRights);
  }

  @Roles(Role.Admin)
  @Get()
  async findAll() {
    return await this.usersAdminService.findAllAdmin();
  }

  @Roles(Role.Admin)
  @Get('/with-deleted')
  async findAllWithDeleted() {
    return await this.usersAdminService.findAllAdminWithDeleted();
  }

  
  @Roles(Role.Admin)
  @Get(':id')
  async findOne(@Param('id',  new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.usersAdminService.findOneAdmin(+id);
  }

  @Roles(Role.Admin)
  @Post('/search')
  async findUserByEmail(@Body(new ValidationPipe()) searchUserByEmailDto: SearchUserByEmailDto) {
    const user = await this.usersAdminService.findOneAdminByEmail(
      searchUserByEmailDto.email,
    );
    return user;
  }


  @Roles(Role.Admin)
  @Patch(':id')
  async update(
    @Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,
    @Body(new ValidationPipe()) updateUserAdminDto: UpdateUserAdminDto,
  ) {
    return await this.usersAdminService.updateAdmin(+id, updateUserAdminDto);
  }

  @Roles(Role.Admin)
  @Delete(':id')
  async remove(
    @Param('id', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.usersAdminService.removeAdmin(+id);
  }
}
