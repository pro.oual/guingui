import { HttpException, HttpStatus } from '@nestjs/common';

export class DuplicateSiretException extends HttpException {
  constructor() {
    super('Le numero de siret est déjà utilisée.', HttpStatus.CONFLICT);
  }
}
