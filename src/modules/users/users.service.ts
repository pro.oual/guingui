import { HttpCode, HttpException, HttpStatus, Inject, Injectable, Req, UnauthorizedException } from '@nestjs/common';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository, DataSource, EntityManager, IsNull } from 'typeorm';
import { Users } from 'src/entities/user.entity';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { UserToRight } from 'src/entities/user_right.entity';
import { Rights } from 'src/entities/right.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateUserAdminDto } from './dto/create-user-admin.dto';
import * as bcrypt from 'bcrypt';
import { UserGoogle } from '../auth/strategies/auth.google.strategy';
import { UserFacebook } from '../auth/strategies/auth.facebook.strategy';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';
import { Role } from 'src/decorators/constants';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { FavorieUser } from 'src/entities/favorie-user.entity';
import { CreateUserFavDto } from './dto/create-user-fav.dto';

import Stripe from 'stripe';
import * as moment from 'moment';
import { DeleteUserFavDto } from './dto/delete-user-fav.dto';
import { ResponsiveUserDto } from './dto/responsive-user.dto';

@Injectable()
export class UsersService {
  private stripe: Stripe;

  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
    @InjectRepository(FavorieUser)
    private favRepository: Repository<FavorieUser>,
    @InjectEntityManager() private readonly entityManager: EntityManager,

  ) {
    this.stripe = new Stripe('sk_test_51NgVRIAyWtQXAtnBGGWMRBRs3PaKPfn8o1W0inMbcb5j6NN0A72Z5rNZBlUrKS1d6cO7xTaZ6ghSnQ2gJJCIUcEd003TeeLtXy', {
      apiVersion: '2023-10-16', // Assurez-vous d'utiliser une version compatible de l'API Stripe
    });
  }

  
  async create(
    createUserDto: CreateUserDto,
  ) : Promise<Users> {
      const salt = await bcrypt.genSalt();
      const password = createUserDto.password;
      const hash = await bcrypt.hash(password, salt);
      createUserDto.created_at =  moment(moment.now()).toDate()
      createUserDto.password = hash;
      createUserDto.salt = salt;
      createUserDto.role = Role.User;
      return  new Users(await this.usersRepository.save(createUserDto))
    }
  
  async findAll() {
    return await this.usersRepository.find({
      where:{
        deleted_at: IsNull()
      }
    });
  }
  async findAllWithDeleted(){
    return await this.usersRepository.createQueryBuilder('u')
    .withDeleted()
    .getMany();
  }


  async findOne(id: number) {
     const user = await this.usersRepository.findOneBy({
      user_id: +id,
      deleted_at: IsNull()
    });
    if(!user){
      throw new HttpException("utilisateur inexistant",404)
    }
    return new Users(user)
  }


  async findOneByEmail(email: string) {
    const user = await this.usersRepository.findOneBy({
      email: email,
      deleted_at: IsNull()
    });

    if(!user){
      throw new HttpException("utilisateur inexistant",404)
    }
    return new Users(user);
  }

  async findOneByEmailForSignIn(email: string) {
    const user = await this.usersRepository.findOneBy({
      email: email,
    });
    if(!user){
      return user
    }
    return new Users(user);
  }


  async findOneBySiret(siret: string) {
    const userBySiret = await this.usersRepository.findOneBy({
      siret: siret,
      deleted_at: IsNull()
    });
    console.log(userBySiret)
    if(!userBySiret){
      return new Users(userBySiret);
    }
    throw new HttpException("Entreprise inexistant",404)

  }


  async update(id: number, updateUserDto: UpdateUserDto, req?: any) {
    updateUserDto.updated_at =  moment(moment.now()).toDate()
    if(req.role === Role.User){
      if(req.user_id !== id){
        throw new UnauthorizedException();
      }
      return await this.usersRepository.update(+req.user_id, updateUserDto);
    }
    return await this.usersRepository.update(+id, updateUserDto);
  }


   
  async remove(id: number, req: any) {
    if(req.role === Role.User ){
      if(req.user_id !== id){
        throw new UnauthorizedException();
      }
      return await this.usersRepository.update(+id, new Users({
        deleted_at: moment(moment.now()).toDate()
      }));
    }
    return await this.usersRepository.update(+id, new Users({
      deleted_at: moment(moment.now()).toDate()
    }));
  }



  async responsiveUser(id: number, req?: any) {
    if(req.role === Role.User){
      if(req.user_id !== id){
        throw new UnauthorizedException();
      }
      return await this.usersRepository.update(+req.user_id, {
        deleted_at: null
      });
    }
    return await this.usersRepository.update(+id,  {
      deleted_at: null
    });
  }


  async makeFav(createFavUserDto:CreateUserFavDto,userConnected : any){   
    const alreadyFav = await this.favRepository.createQueryBuilder('fav')
    .where('fav.service = :favservice',{ favservice:createFavUserDto.service } )
    .andWhere('fav.user = :favuser', {favuser: userConnected.user_id})
    .getOne();
    if(alreadyFav){
      throw new HttpException("Ce service et deja dans vos favoris",HttpStatus.NOT_FOUND)
    }
    return this.favRepository.save(new FavorieUser(createFavUserDto))
  }

  async deleteFav(deleteFavUserDto:DeleteUserFavDto, userConnected: any){
    const alreadyFav = await this.favRepository.createQueryBuilder('fav')
    .where('fav.service = :favservice',{ favservice:deleteFavUserDto.service } )
    .andWhere('fav.user = :favuser', {favuser: userConnected.user_id})
    .getOne();
    if(alreadyFav){
      return this.entityManager.delete(FavorieUser,new FavorieUser(deleteFavUserDto))   
    }
    throw new HttpException("Ce service n'est pas dans vos favoris",HttpStatus.NOT_FOUND) 
  }


/*EXPERIMNENTALE*/
  async createSellerAccount(email: string): Promise<Stripe.Account> {
    const account = await this.stripe.accounts.create({
      type: 'express',
      business_type: 'individual',
      email,
    });
    return account;
  }

  async addCreditCardToSeller(accountId: string, cardToken: string): Promise<Stripe.Card> {
    const externalAccount = await this.stripe.accounts.createExternalAccount(accountId, {
      external_account: cardToken,
      
    });
  
    // Vérifiez si l'externalAccount est une carte de crédit (Card)
    if (externalAccount.object === 'card') {
      // Le résultat est une carte de crédit, vous pouvez le traiter comme tel
      const card = externalAccount as Stripe.Card;
      return card;
    } else {
      // Le résultat n'est pas une carte de crédit, vous pouvez gérer cette situation ici
      throw new Error('Le résultat n\'est pas une carte de crédit');
    }
  }
  async createPaymentIntent(amount: number, customerId: string, accountId: string): Promise<Stripe.PaymentIntent> {
    const paymentIntent = await this.stripe.paymentIntents.create({
      amount,
      currency: 'eur', // Remplacez par votre devise
      customer: customerId,
      transfer_data: {
        destination: accountId,
      },
    });
    return paymentIntent;
  }
/**END EXPERIMENTALE */

}

export interface Favoris{
  service: number,
  user: number
}
