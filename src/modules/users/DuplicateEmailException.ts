import { HttpException, HttpStatus } from '@nestjs/common';

export class DuplicateEmailException extends HttpException {
  constructor() {
    super('L\'adresse e-mail est déjà utilisée.', HttpStatus.CONFLICT);
  }
}
