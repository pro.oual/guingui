import { Transform } from 'class-transformer';
import {
  IsArray,
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import * as moment from 'moment';
import { userRights } from '../users.admin.service';
import { UserToRight } from 'src/entities/user_right.entity';

export class CreateUserAdminDto {
  @IsNotEmpty()
  @IsString()
  first_name: string;

  @IsNotEmpty()
  @IsString()
  last_name: string;

  @IsNotEmpty()
  @IsString()
  username: string;

  @IsNotEmpty()
  @IsString()
  phone_number: string;

  @IsNotEmpty()
  @IsString()
  number_address: string;
  
  @IsNotEmpty()
  @IsString()
  address: string;
  
  @IsNotEmpty()
  @IsString()
  zip_code: string;

  @IsNotEmpty()
  @IsString()
  city: string;

  @IsNotEmpty()
  @IsString()
  genre: string;
  
  @IsNotEmpty()
  //@IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  date_of_birth: Date;

  @IsBoolean()
  @IsOptional()
  is_active: boolean;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsBoolean()
  @IsOptional()
  email_verified: boolean;

  @IsString()
  @IsOptional()
  local: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsString()
  @IsOptional()
  salt: string;

  //@IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  @IsOptional()
  last_seen: Date;

  @IsBoolean()
  @IsOptional()
  is_blocked: boolean;

  @IsString()
  @IsOptional()
  block_reason: string;

  @IsOptional()
  avatar: any;

  @IsString()
  @IsOptional()
  token_password_forget: string;

  //@IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  @IsOptional()
  email_verified_at: Date;

  @IsBoolean()
  @IsNotEmpty()
  general_conditions: boolean;


  @IsString()
  @IsNotEmpty()
  iban: string;

  @IsString()
  @IsNotEmpty()
  bic: string;

  @IsString()
  @IsOptional()
  siret: string;

  @IsString()
  @IsOptional()
  facebook: string;

  @IsString()
  @IsOptional()
  instagram: string;

  @IsString()
  @IsOptional()
  pinterest: string;

  @IsString()
  @IsOptional()
  youtube: string;

//  @IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  @IsOptional()
  mobilite: boolean;

  @IsString()
  @IsOptional()
  number_address_mobilite_entreprise: string;

  @IsString()
  @IsOptional()
  address_mobilite_entreprise: string;

  @IsString()
  @IsOptional()
  zip_code_mobilite_entreprise: string;

  @IsString()
  @IsOptional()
  city_mobilite_entreprise: string;

  //@IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  created_at: Date;

  //@IsDate()
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  @IsOptional()
  updated_at: Date;

  //@IsDate()
  
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  @IsOptional()
  deleted_at: Date;

  @IsBoolean()
  @IsNotEmpty()
  is_entreprise: boolean;

  role: string;

  @IsNotEmpty()
  @IsArray()
  userToRight: userRights[]
}
