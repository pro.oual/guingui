import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { Service } from 'src/entities/service.entity';
import { Users } from 'src/entities/user.entity';

export class DeleteUserFavDto {

  @IsNotEmpty()  
  service: number;
  @IsNotEmpty()  
  user: number
}
