import { IsEmail, IsString } from 'class-validator';

export class SearchUserByEmailDto {
  @IsString()
  @IsEmail()
  email: string;
}
