import { Transform } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import * as moment from 'moment';
import { Service } from 'src/entities/service.entity';
import { Users } from 'src/entities/user.entity';

export class ResponsiveUserDto {

  @IsNotEmpty()  
  @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
  deleted_at: Date
}
