import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from 'src/entities/user.entity';
import { RightsModule } from '../rights/rights.module';
import { AdminController } from './users.admin.controller';
import { UsersAdminService } from './users.admin.service';
import { UserToRight } from 'src/entities/user_right.entity';
import { BullModule } from '@nestjs/bull';
import { join } from 'path';
import { FavorieUser } from 'src/entities/favorie-user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Users, UserToRight, FavorieUser]), RightsModule
  ],
  controllers: [UsersController, AdminController],
  providers: [UsersService, UsersAdminService],
  exports: [TypeOrmModule, UsersService],
})
export class UsersModule {}
