import { Test, TestingModule } from '@nestjs/testing';
import { OptionTypeCategorieServiceController } from './option-type-categorie-service.controller';
import { OptionTypeCategorieServiceService } from './option-type-categorie-service.service';

describe('OptionTypeCategorieServiceController', () => {
  let controller: OptionTypeCategorieServiceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OptionTypeCategorieServiceController],
      providers: [OptionTypeCategorieServiceService],
    }).compile();

    controller = module.get<OptionTypeCategorieServiceController>(OptionTypeCategorieServiceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
