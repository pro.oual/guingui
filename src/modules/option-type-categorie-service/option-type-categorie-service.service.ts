import { HttpException, Injectable } from '@nestjs/common';
import { CreateOptionTypeCategorieServiceDto } from './dto/create-option-type-categorie-service.dto';
import { UpdateOptionTypeCategorieServiceDto } from './dto/update-option-type-categorie-service.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OptionTypeCategorieService } from '../../entities/option-type-categorie-service.entity';
import { DataSource, Repository } from 'typeorm';
import { ServiceService } from '../service/service.service';

@Injectable()
export class OptionTypeCategorieServiceService {
 

  constructor(
    @InjectRepository(OptionTypeCategorieService)
    private optionTypeCategorieServiceRepository: Repository<OptionTypeCategorieService>,
    private dataSource: DataSource,
  ) {}


/*  async create(createOptionTypeCategorieServiceDto: CreateOptionTypeCategorieServiceDto) {

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {

      const idService = createOptionTypeCategorieServiceDto.service_id
      const service = this.servService.findOne(+idService)



      const newUser = new Users(CreateUserAdminDto);
      newUser.password = await bcrypt.hash(CreateUserAdminDto.password, salt);
      newUser.salt = salt;
      newUser.role = Role.Admin;
      newUser.created_at =  moment(moment.now()).toDate()
      await queryRunner.manager.save(newUser);
      CreateUserAdminDto.userToRight.forEach(async (element, index) => {
      const right = await queryRunner.manager.findOneBy(Rights, {
        rights_id: element.rights_id
      });
      const userRight = new UserToRight({
        user:newUser,
        right: right
      })
      await queryRunner.manager.save(userRight);
      }
      );
      await queryRunner.commitTransaction();
      return {
        status: HttpStatus.CREATED,
        message: 'Admin cree avec succes',
      };
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw new InternalServerErrorException("Une erreur est survenue.");
    } finally {
      await queryRunner.release();
    }

   return this.optionTypeCategorieServiceRepository.save(createOptionTypeCategorieServiceDto)
  }
*/
  findAll() {
    return this.optionTypeCategorieServiceRepository.find();
  }

  async findOne(id: number) {
    const opts = await this.optionTypeCategorieServiceRepository.createQueryBuilder('otcs')
    .leftJoinAndSelect("otcs.service", "otcss")
    .leftJoinAndSelect("otcs.option", "otcso")
    .where('otcs.OptionTypeCategorieService_id = :tc', {tc: +id})
    .getOne();
    return opts;
  }

  
  
  async findwithRelation(){
    const opts = await this.optionTypeCategorieServiceRepository.createQueryBuilder('opt')
    .leftJoinAndSelect("opt.option", "optc")
    .leftJoinAndSelect("opt.service", "opttc")
    .getMany();
    return opts;
  }


  

  update(id: number, updateOptionTypeCategoryServiceDto: UpdateOptionTypeCategorieServiceDto) {
    return this.optionTypeCategorieServiceRepository.update(
      +id,
      updateOptionTypeCategoryServiceDto
    );
  }

  remove(id: number) {
    return this.optionTypeCategorieServiceRepository.delete(
      +id
    );
  }




}
