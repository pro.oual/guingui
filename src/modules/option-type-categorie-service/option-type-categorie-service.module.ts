import { Module } from '@nestjs/common';
import { OptionTypeCategorieServiceService } from './option-type-categorie-service.service';
import { OptionTypeCategorieServiceController } from './option-type-categorie-service.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OptionTypeCategorieService } from 'src/entities/option-type-categorie-service.entity';

@Module({
  imports:[TypeOrmModule.forFeature([OptionTypeCategorieService])],
  controllers: [OptionTypeCategorieServiceController],
  providers: [OptionTypeCategorieServiceService],
})
export class OptionTypeCategorieServiceModule {}
