import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, ParseIntPipe, HttpStatus } from '@nestjs/common';
import { OptionTypeCategorieServiceService } from './option-type-categorie-service.service';
import { CreateOptionTypeCategorieServiceDto } from './dto/create-option-type-categorie-service.dto';
import { UpdateOptionTypeCategorieServiceDto } from './dto/update-option-type-categorie-service.dto';
import { Role } from 'src/decorators/constants';
import { Public, PublicRole } from 'src/decorators/public.decorator';
import { Roles } from 'src/decorators/roles.decorator';

@Controller('option-type-categorie-service')
export class OptionTypeCategorieServiceController {
  constructor(private readonly optionTypeCategorieServiceService: OptionTypeCategorieServiceService) {}

  /*@Roles(Role.Admin) pa&s besoin de la methode create puisque on cree un service puis on affecte se service dans la table otcs
  @Post()
  async create(@Body(new ValidationPipe()) createOptionTypeCategorieServiceDto: CreateOptionTypeCategorieServiceDto) {
    return await this.optionTypeCategorieServiceService.create(createOptionTypeCategorieServiceDto);
  }*/

  
  @Public()
  @PublicRole()
  @Get()
  findAll() {
    return this.optionTypeCategorieServiceService.findwithRelation();
  }

  
  @Public()
  @PublicRole()
  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.optionTypeCategorieServiceService.findOne(+id);
  }
  @Roles(Role.Admin)
  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateOptionTypeCategorieServiceDto: UpdateOptionTypeCategorieServiceDto) {
    return this.optionTypeCategorieServiceService.update(+id, updateOptionTypeCategorieServiceDto);
  }
  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.optionTypeCategorieServiceService.remove(+id);
  }
}
