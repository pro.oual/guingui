import { PartialType } from '@nestjs/mapped-types';
import { CreateOptionTypeCategorieServiceDto } from './create-option-type-categorie-service.dto';

export class UpdateOptionTypeCategorieServiceDto extends PartialType(CreateOptionTypeCategorieServiceDto) {}
