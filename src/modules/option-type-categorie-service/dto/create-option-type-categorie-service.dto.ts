import { Transform } from "class-transformer";
import { IsDate, IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";
import * as moment from "moment";


export class CreateOptionTypeCategorieServiceDto {

    @IsString()
    @IsNotEmpty()
    name: string;
  
    @IsString()
    @IsNotEmpty()
    description: string;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    deleted_at: Date;
  
    @IsInt()
    service_id: number;
  
    @IsInt()
    @IsOptional()
    optionTypeCategorie_id: number;
}
