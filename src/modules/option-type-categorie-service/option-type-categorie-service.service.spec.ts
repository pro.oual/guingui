import { Test, TestingModule } from '@nestjs/testing';
import { OptionTypeCategorieServiceService } from './option-type-categorie-service.service';

describe('OptionTypeCategorieServiceService', () => {
  let service: OptionTypeCategorieServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OptionTypeCategorieServiceService],
    }).compile();

    service = module.get<OptionTypeCategorieServiceService>(OptionTypeCategorieServiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
