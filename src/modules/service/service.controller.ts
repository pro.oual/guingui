import { Controller, Get, Post, Body, Patch, Param, Delete, ValidationPipe, ParseIntPipe, HttpStatus, ClassSerializerInterceptor, UseInterceptors } from '@nestjs/common';
import { ServiceService } from './service.service';
import { CreateServiceDto } from './dto/create-service.dto';
import { UpdateServiceDto } from './dto/update-service.dto';
import { Public, PublicRole } from 'src/decorators/public.decorator';
import { User } from 'src/decorators/user.decorator';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';
import { AcceptServiceDto } from './dto/accept-service.dto';

@Controller('service')
@UseInterceptors(ClassSerializerInterceptor)

export class ServiceController {
  constructor(private readonly serviceService: ServiceService) {}
  
  
  @Public()
  @PublicRole()
  @Post()
  create(@Body(new ValidationPipe()) createServiceDto: CreateServiceDto) {
    return this.serviceService.create(createServiceDto);
  }


  @Public()
  @PublicRole()
  @Get()
  findAll() {
    return this.serviceService.findwithRelation();
  }

  @Public()
  @PublicRole()
  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.serviceService.findOne(+id);
  }
 
  @Public()
  @PublicRole()
  @Get('/all/users')
  async findServiceByAllUser() {
    return await this.serviceService.findServiceByAllUser();
  }


  @Public()
  @PublicRole()
  @Get('/accepted/all/users')
  async findServiceByAllUserAccepted() {
    return await this.serviceService.findServiceByAllUserAccepted();
  }


  @Roles(Role.Admin)
  @Get('/pending/all/users')
  async findServiceByAllUserPending() {
    return await this.serviceService.findServiceByAllUserPending();
  }


  @Public()
  @PublicRole()
  @Get('/accepted/request/all/users')
  async findServiceByAllUserRequestAccepted() {
    return await this.serviceService.findServiceByAllUserRequestAccepted();
  }

  @Public()
  @PublicRole()
  @Get('/accepted/sell/all/users')
  async findServiceByAllUserSellAccepted() {
    return await this.serviceService.findServiceByAllUserSellAccepted();
  }

  @Roles(Role.Admin)
  @Get('/pending/request/all/users')
  async findServiceByAllUserRequestPending() {
    return await this.serviceService.findServiceByAllUserRequestPending();
  }

  @Roles(Role.Admin)
  @Get('/pending/sell/all/users')
  async findServiceByAllUserSellPending() {
    return await this.serviceService.findServiceByAllUserSellPending();
  }

  
  @Public()
  @PublicRole()
  @Get('/sell/by-user/:id')
  async findServiceSellByAllUser(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.serviceService.findServiceSellByUser(+id);
  }

  @Public()
  @PublicRole()
  @Get('/request/by-user/:id')
  async findServiceRequestByAllUser(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return await this.serviceService.findServiceRequestByUser(+id);
  }


  
  @Public()
  @PublicRole()
  @Get('/by-user/:id')
  async findServiceByUser(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,) {
    return await this.serviceService.findServiceByUser(+id);
  }

  @Roles(Role.Admin)
  @Patch('/accept/:id')
  async acceptService(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,@Body(new ValidationPipe()) acceptServiceDto: AcceptServiceDto) {
    return await this.serviceService.acceptService(+id,acceptServiceDto);
  }

  @Roles(Role.User,Role.Admin)
  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string,@Body(new ValidationPipe()) updateServiceDto: UpdateServiceDto) {
    return this.serviceService.update(updateServiceDto, +id);
  }

  @Roles(Role.User,Role.Admin)
  @Delete(':id')
  remove(@User() user: any, @Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.serviceService.remove(+user.user_id,+id);
  }
}
