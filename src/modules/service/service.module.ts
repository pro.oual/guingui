import { Module } from '@nestjs/common';
import { ServiceService } from './service.service';
import { ServiceController } from './service.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from 'src/entities/service.entity';
import { QuestionAnswerService } from '../question-answer/question-answer.service';
import { QuestionAnswer } from 'src/entities/question-answer.entity';
import { DurationInfoService } from 'src/entities/duration-info-service.entity';
import { OptionTypeCategorieService as serv } from '../option-type-categorie/option-type-categorie.service';
import { OptionTypeCategorieServiceService } from '../option-type-categorie-service/option-type-categorie-service.service';
import { OptionTypeCategorie } from 'src/entities/option-type-categorie.entity';
import { OptionTypeCategorieService } from 'src/entities/option-type-categorie-service.entity';

@Module({
  imports:[TypeOrmModule.forFeature([Service, QuestionAnswer, DurationInfoService, OptionTypeCategorie, OptionTypeCategorieService])],
  controllers: [ServiceController],
  providers: [ServiceService,QuestionAnswerService, serv, OptionTypeCategorieServiceService],
})
export class ServiceModule {}
