import { HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateServiceDto } from './dto/create-service.dto';
import { UpdateServiceDto } from './dto/update-service.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Service } from 'src/entities/service.entity';
import { And, DataSource, EntityManager, Repository } from 'typeorm';
import { Users } from 'src/entities/user.entity';
import { QuestionAnswer } from 'src/entities/question-answer.entity';
import { QuestionAnswerService } from '../question-answer/question-answer.service';
import { CreateQuestionAnswerDto } from '../question-answer/dto/create-question-answer.dto';
import { DurationInfoService } from 'src/entities/duration-info-service.entity';
import { CreateDurationInfoServiceDto } from '../duration-info-service/dto/create-duration-info-service.dto';
import { DurationInfoServiceService } from '../duration-info-service/duration-info-service.service';
import { OptionTypeCategorieService } from 'src/entities/option-type-categorie-service.entity';
import { OptionTypeCategorieServiceService } from '../option-type-categorie-service/option-type-categorie-service.service';
import { OptionTypeCategorieService as serv} from '../option-type-categorie/option-type-categorie.service';
import * as moment from 'moment';

@Injectable()
export class ServiceService {
  constructor(
    @InjectRepository(Service)
    private serviceRepository: Repository<Service>,
    @InjectRepository(QuestionAnswer)
    private QARepository: Repository<QuestionAnswer>,
    @InjectRepository(DurationInfoService)
    private DISRepository: Repository<DurationInfoService>,
    @InjectRepository(OptionTypeCategorieService)
    private OTCSRepository: Repository<OptionTypeCategorieService>,
    @InjectEntityManager() private readonly entityManager: EntityManager,
    private dataSource: DataSource,
    private optionTypeCS: serv
  ) {}


    async create(
      createServiceDto: CreateServiceDto) {
      const queryRunner = this.dataSource.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
      try {
        //TODO : faire une verification sur le montant reçu
        const serviceEntity = new Service(createServiceDto)
        const qAServiceEntity = new QuestionAnswer(createServiceDto.qa)
        const serviceRegistered = await queryRunner.manager.save(serviceEntity);
        qAServiceEntity.service = serviceRegistered
        await queryRunner.manager.save(qAServiceEntity);
        await Promise.all(
          createServiceDto.dis.map(async (element, key) => {
            const durationService = new DurationInfoService(element);
            durationService.service = serviceRegistered;
            await queryRunner.manager.save(durationService);
          })
        );
        await Promise.all(
          createServiceDto.optionsType.map(async (element, key) => {
              console.log(element.OptionTypeCategorie_id)
              const option = await this.optionTypeCS.findOne(element.OptionTypeCategorie_id as number)
              await queryRunner.manager.save(new OptionTypeCategorieService({
                option:option,
                service:serviceRegistered
              }));
          })
        );
        


        await queryRunner.commitTransaction();
        return {
          status: HttpStatus.CREATED,
          message: 'Service cree avec succes',
        };
      } catch (err) {
        await queryRunner.rollbackTransaction();
        throw new InternalServerErrorException("Une erreur est survenue."+err);
      } finally {
        await queryRunner.release();
      }
  

  }


  
  async findAll() {
    return this.serviceRepository.find();
  }

  findOne(id: number) {
    return this.serviceRepository.findBy({
      service_id: +id
    });
  }
  
  async findwithRelation(){
    const opts = await this.serviceRepository.createQueryBuilder('srv')
    .leftJoinAndSelect("srv.user", "srvuser")
    .getMany();
    return opts;
  }

  async findServiceByUser(id: number){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.user = :user',{user:+id})
    .getMany();

    //console.log(service)
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }

  async findAllWithRelation(id: number){
      return this.findServiceByUser(+id)
  }

  async findServiceByAllUser(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    return allRelation;
  }

  async findServiceByAllUserAccepted(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.valid_back_office = true')
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }





  async findServiceByAllUserRequestAccepted(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.valid_back_office = true')
    .andWhere("srv.type_of_service = 'R'")
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }


  async findServiceByAllUserSellAccepted(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.valid_back_office = true')
    .andWhere("srv.type_of_service = 'S'")
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }


  async findServiceByAllUserRequestPending(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.valid_back_office = false')
    .andWhere("srv.type_of_service = 'R'")
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }
  async findServiceByAllUserSellPending(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where('srv.valid_back_office = false')
    .andWhere("srv.type_of_service = 'S'")
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }
  
  async findServiceByAllUserPending(){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .leftJoinAndSelect("srv.user", "srvuser")
    .where('srv.valid_back_office = false')
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    console.log(allRelation.length)
    return allRelation;
  }




  async findServiceSellByUser(id:number){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where("srv.type_of_service = 'S'")
    .andWhere('srv.user = :user',{user:+id})
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    return allRelation;
  }


  async findServiceRequestByUser(id:number){
    const service = await this.serviceRepository.createQueryBuilder('srv')
    .where("srv.type_of_service = 'R'")
    .andWhere('srv.user = :user',{user:+id})
    .getMany();
    let allRelation = []
    await Promise.all(
      service.map(async (element, key) => {
          console.log(element)

          const qa = await this.QARepository.createQueryBuilder('qa')
          .where('qa.service = :serv',{serv:element.service_id})
          .getMany();

          const dis = await this.DISRepository.createQueryBuilder('dis')
          .where('dis.service = :serv',{serv:element.service_id})
          .getMany();
          const otcs = await this.OTCSRepository.createQueryBuilder('otcs')
          .where('otcs.service = :serv',{serv:element.service_id})
          .getMany();
          allRelation.push(
            {
              element,
              qa,
              dis,
              otcs
            }
          );
      })
    );
    return allRelation;
  }



  acceptService(id:number,AcceptServiceDto){
    return this.serviceRepository.update(
      +id,
      AcceptServiceDto
    );
  }


  update( updateServiceDto: UpdateServiceDto, id: number) {
    return this.serviceRepository.update(
      +id,
      updateServiceDto
    );
  }

  remove(user_id: number, id: number) {
    return this.serviceRepository.update(+id,
      {
        deleted_at: moment(moment.now()).toDate()
      }
      
    );
  }
}
