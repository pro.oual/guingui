import { Transform, Type } from "class-transformer";
import { IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { DurationInfoService } from "src/entities/duration-info-service.entity";
import { OptionTypeCategorie } from "src/entities/option-type-categorie.entity";
import { Service } from "src/entities/service.entity";
import { Users } from "src/entities/user.entity";
import { CreateQuestionAnswerDto } from "src/modules/question-answer/dto/create-question-answer.dto";

export class CreateServiceDto {

    @IsNumber()
    @IsNotEmpty()
    total_amount_service: number;

    @IsNumber()
    @IsNotEmpty()
    already_paid: number;
    
    @IsNumber()
    @IsNotEmpty()
    amount_want_sell: number;
    
    @IsNumber()
    @IsNotEmpty()
    new_price: number;
    
    @IsBoolean()
    @IsOptional()
    provider_accepted: boolean;
    
        
    @IsBoolean()
    @IsNotEmpty()
    have_balance: boolean;
           
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    date_validity_balance: Date;
    //avoir important
    @IsString()
    @IsNotEmpty()
    url_balance: string;

    @IsString()
    @IsNotEmpty()
    provider_name: string;

    @IsString()
    @IsOptional()
    email_provider: string;
    
    @IsString()
    @IsOptional()

    phone_number: string;
    
    @IsString()
    @IsOptional()
    
    web_site_provider: string;
    
    @IsOptional()
    @IsString()
    instagram_provider: string;
    
    @IsOptional()
    @IsString()
    facebook_provider: string;


    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string;

    @IsString()
    @IsNotEmpty()
    img1: string;
    @IsString()
    @IsNotEmpty()
    img2: string;
    @IsString()
    @IsNotEmpty()
    img3: string;
    @IsString()
    @IsNotEmpty()
    img4: string;
        
    @IsBoolean()
    @IsNotEmpty()
    valid: boolean;
        
    @IsBoolean()
    @IsNotEmpty()
    valid_back_office: boolean;

    @IsString()
    @IsOptional()

    name_of_offer: string;

    @IsString()
    @IsOptional()
    type_of_service: string;

    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    created_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    updated_at: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())
    @IsOptional()
    deleted_at: Date;

    @IsInt()
    @IsNotEmpty()
    user: Users;

    @Type(() => OptionTypeCategorie) // Pour indiquer que qa est de type CreateQADto
    optionsType: OptionTypeCategorie[];


    @Type(() => CreateQuestionAnswerDto) // Pour indiquer que qa est de type CreateQADto
    qa: CreateQuestionAnswerDto;

    
    @Type(() => DurationInfoService) // Pour indiquer que qa est de type CreateQADto
    dis: DurationInfoService[];
}
