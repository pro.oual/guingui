import { Transform, Type } from "class-transformer";
import { IsBoolean, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { DurationInfoService } from "src/entities/duration-info-service.entity";
import { OptionTypeCategorie } from "src/entities/option-type-categorie.entity";
import { Service } from "src/entities/service.entity";
import { Users } from "src/entities/user.entity";
import { CreateQuestionAnswerDto } from "src/modules/question-answer/dto/create-question-answer.dto";

export class AcceptServiceDto {


    @IsBoolean()
    @IsNotEmpty()
    valid_back_office: boolean;

   
}
