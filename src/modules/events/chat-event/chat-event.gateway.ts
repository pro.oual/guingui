import { ConnectedSocket, MessageBody, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { RemoteSocket, Server, Socket } from 'socket.io';
import * as jwt from 'jsonwebtoken';
import { InjectQueue, OnGlobalQueueActive, OnQueueActive } from '@nestjs/bull';
import { Job, Queue } from 'bull';
import { ChatService } from 'src/modules/chat/chat.service';
import { UsersService } from 'src/modules/users/users.service';


@WebSocketGateway(3001, { 
  transports: ['polling','websocket'],
  cors: {
    origin: '*',
  }
})
export class ChatEventGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;
  
  

  private userConnections: Map<string, Socket> = new Map();
 // private userNotification:any
  
  constructor(
    @InjectQueue('message') private messageQueue: Queue,
    private chatService: ChatService,
    private userService: UsersService,

    //private readonly queue: Queue
  ) {}
  @SubscribeMessage('message')
  async handleMessage(@ConnectedSocket() client: Socket , @MessageBody() payload: {  
    message: string,
    sender: number,
    recipient: number,
    room: string 
  
  }) {
      await this.chatService.create(
        {
          content: payload.message,
          file_url: '',
          sender:  await this.userService.findOne(payload.sender),
          receiver: await this.userService.findOne(payload.recipient)
        }
      )
      const foundElementRecipient = this.getElementByUserId(this.server.sockets.adapter.rooms, +payload.recipient);
      if(!foundElementRecipient){
        await this.messageQueue.add({
            payload
        })
      }
      const roomSure = {
        key: ''
      };
      const flag = this.server.sockets.adapter.rooms.has(this.reverseString(payload.room))
      if(flag){
        this.server.sockets.adapter.rooms.forEach((element, key, map)=>{
            if(key === this.reverseString(payload.room)){
              roomSure.key = key;
            }
        })
        await this.server.to(roomSure.key).emit('new_msg', payload);
      }else{
        await this.server.to(payload.room).emit('new_msg', payload);
      }
  }

  @SubscribeMessage('joinRoom')
  async handleJoinRoom(@ConnectedSocket() client: Socket, @MessageBody() payload: { sender:number,
    recipient:number, room: string}) {
      const roomSure = {
        key: ''
      };
      const foundElementRecipient = this.getElementByUserId(this.server.sockets.adapter.rooms, +payload.recipient);
      const allSocket = await this.server.sockets.fetchSockets()
      const flag = this.server.sockets.adapter.rooms.has(this.reverseString(payload.room))
      if(flag){
        this.server.sockets.adapter.rooms.forEach((element, key, map)=>{
            if(key === this.reverseString(payload.room)){
              roomSure.key = key;
            }
        })
        //
        client.join(roomSure.key)
        
        for (const socket of allSocket) {
          if(foundElementRecipient.socketId === socket.id ){
            socket.join(roomSure.key)
          }
        } 
      }else{
        client.join(payload.room)

        for (const socket of allSocket) {
          if(foundElementRecipient.socketId === socket.id ){
            socket.join(payload.room)
          }
        }
      }

      console.log(this.server.sockets.adapter.rooms)

    
  }



 
  afterInit(server: Server) {
    //console.log(server);
  }


  async handleConnection(
    @ConnectedSocket() client: Socket,
    ...args: any[]) {
        const token = client.handshake.auth.token; // Récupérer le token JWT de l'en-tête
        try {
          const decodedToken:any = await jwt.verify(token, process.env.SECRET_JWT); 
          client.handshake.headers['user'] = JSON.stringify(decodedToken)
          const sid = this.server.sockets.adapter.rooms.get([client.id][0])
          sid['user_id'] = decodedToken.user_id

          this.userConnections.set(decodedToken.user_id.toString(),client)  
          console.log('User authenticated:', decodedToken.user_id);
         /* const jobs = await this.getJobsInQueue(decodedToken.user_id)
        /  if(jobs){
            jobs.forEach(async (element,key)=>{
              await client.emit('new_msg', element.data.payload.message)
          })
          await this.deleteJobsByCriteria(decodedToken.user_id)
          }*/
         console.log(this.server.sockets.adapter.rooms)
        } catch (err) {
          console.error('Unauthorized access:', err.message);
          client.disconnect(true); // Déconnexion du client non authentifié
        }
        
  }


  handleDisconnect(@ConnectedSocket() client: Socket) {
    // Trouvez et supprimez le client déconnecté de la liste des clients
    const disconnectedUserId = [...this.userConnections.entries()].find(([userId, socket]) => socket === client)?.[0];
    if (disconnectedUserId) {
      this.userConnections.delete(disconnectedUserId);
      console.log(`Disconnected: ${client.id} (User: ${disconnectedUserId})`);
    }
  }


  getElementByUserId(userToSocketMap, userId) {
    for (const [socketId, socketInfo] of userToSocketMap) {

        console.log("socketId2", socketId, socketInfo['user_id'])
        console.log("user",userId)

      if (socketInfo['user_id'] === userId) {
        return { socketId, socketInfo };
      }
    }
    return null; // Rien trouvé
  }

  async getJobsInQueue(user_id: number): Promise<Job[]> {
    const jobs = await this.messageQueue.getJobs(['completed', 'failed']);
    const resultJobs = jobs.filter((element) => {
      return element.data.payload.recipient === user_id;
    });
    return resultJobs;
  }

  async deleteJobsByCriteria(user_id: number): Promise<void> {
    const jobs = await this.messageQueue.getJobs(['completed', 'failed']);
    for (const job of jobs) {
      if (job.data.payload.recipient === user_id) {
        await job.remove();
      }
    }
  }
  reverseString(input: string): string {
    const parts = input.split('-');
    if (parts.length === 2) {
      // Inverse les parties et les réassemble avec un tiret
      return `${parts[1]}-${parts[0]}`;
    } else {
      // Gère le cas où la chaîne n'a pas le format attendu
      return input;
    }
  }
}


