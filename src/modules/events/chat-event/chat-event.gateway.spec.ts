import { Test, TestingModule } from '@nestjs/testing';
import { ChatEventGateway } from './chat-event.gateway';

describe('ChatEventGateway', () => {
  let gateway: ChatEventGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChatEventGateway],
    }).compile();

    gateway = module.get<ChatEventGateway>(ChatEventGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
