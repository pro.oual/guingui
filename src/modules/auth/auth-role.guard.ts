import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from 'src/decorators/constants';
import { ROLES_KEY } from 'src/decorators/roles.decorator';
import { IS_PUBLIC_ROLE_KEY } from 'src/decorators/public.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isPublicRole = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_ROLE_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        if (isPublicRole) {
            return true;
        }
        const rolesDefined = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        const userRole = context.switchToHttp().getRequest()['user'];

        if (!userRole) {
            return false; // L'utilisateur n'est pas authentifié
        }

        const userRoles = userRole.role; 
        return rolesDefined.some((requiredRole) => userRoles.includes(requiredRole));
        
        
    }
    
}
