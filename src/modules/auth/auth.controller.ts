import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  Res,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateLoginDto } from './dto/create-login.dto';
import { NewPasswordDto } from './dto/new-password.dto';
import { GetEmailDto } from './dto/get-email.dto';
import { Public, PublicReactive, PublicRole } from 'src/decorators/public.decorator';
import { User } from 'src/decorators/user.decorator';
import { ReactiveGuard } from './auth-reactive.guard';
@Controller('auth')
@UseInterceptors(ClassSerializerInterceptor)

export class AuthController {
  constructor(
    private authService: AuthService,
  ) {}
  
  @Public()
  @PublicRole()
  @Post('login')
  async signIn(@Body() createLoginDto: CreateLoginDto) {
    return await this.authService.signIn(createLoginDto);
  }

  @Public()
  @PublicRole()
  @Post('send-verify-password')
  async sendNewPasswordMail(@Body() getEmailDto: GetEmailDto) {
   return await this.authService.sendVerifyPassword(getEmailDto.email)  
  }

  @Public()
  @PublicRole()
  @Post('update-password-forgot')
  async sendNewPassword(@Body() newPasswordDto: NewPasswordDto) {
    return await this.authService.updatePasswordForgot(newPasswordDto);
  }

  @Public()
  @PublicRole()
  @Get('verify-token/:token')
  async verifyToken(@Param('token', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) token: string ) {
    return await this.authService.decodeTokenJwt(token);
  }



}
