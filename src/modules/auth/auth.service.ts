import { ForbiddenException, HttpCode, HttpException, HttpStatus, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { CreateLoginDto } from './dto/create-login.dto';
import { NewPasswordDto } from './dto/new-password.dto';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { MailService } from '../mail/mail.service';
import { Response } from 'express';
import { Role } from 'src/decorators/constants';
import { Users } from 'src/entities/user.entity';
import { User } from 'src/decorators/user.decorator';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private mailService: MailService
  ) {}

  async signIn(createLoginDto: CreateLoginDto): Promise<any> {
    const user = await this.usersService.findOneByEmailForSignIn(createLoginDto.email);
    if (!user || (user.deleted_at != null || user.deleted_at != undefined )) {
      throw new HttpException( "Utilisateur introuvable", HttpStatus.NOT_FOUND);
    }
    if (user.role === Role.Admin && (user.deleted_at != null || user.deleted_at != undefined )) {
      throw new ForbiddenException();
    }
    try {
      const isMatch = await bcrypt.compare(
        createLoginDto.password,
        user.password,
      );
      if (isMatch) {
        const payload = {
          user_id: user.user_id,
          username: user.username,
          role: user.role,
          utility: 'auth',
          created: user.created_at,
          deleted: user.deleted_at
        };
        const access_token = await this.jwtService.signAsync(payload,{
          secret: process.env.SECRET_JWT,
          expiresIn: '8h'
        });
      
        return {
          status:HttpStatus.OK, 
          access_token,
          user
        };
      } else {
        throw new UnauthorizedException('Mot de passe incorrect');
      }
    } catch (err) {
      throw new UnauthorizedException('Mot de passe incorrect');
    }
  }


  async decodeTokenJwt(token: string) {
    const decodedToken =  this.jwtService.verifyAsync(
      token,
      {
        secret: process.env.SECRET_JWT,
      },
    );
    return decodedToken;
  }


  async updatePasswordForgot(newPasswordDto: NewPasswordDto): Promise<any> {

    try {
      const decodedToken = await this.jwtService.verifyAsync(
        newPasswordDto.token,
        {
          secret: process.env.SECRET_JWT,
        },
      );

      const user = await this.usersService.findOne(decodedToken.user_id);
      if (!user.token_password_forget) {
        throw new HttpException("la demande de reinitialisation n'est pas faite", HttpStatus.NOT_FOUND);
      }
      if(user.token_password_forget !== newPasswordDto.token){
        throw new HttpException("la demande ne peut aboutir veuillez recommencez la procédure", HttpStatus.BAD_REQUEST);
      }
      const userId = decodedToken.user_id;
      const salt = await bcrypt.genSalt();
      const hash = await bcrypt.hash(newPasswordDto.password, salt);

      const updateUserPassword: UpdateUserDto = {
        password: hash,
        salt: salt,
      };
      await this.usersService.update(user.user_id, updateUserPassword, user);
      try{
        await this.mailService.sendUserConfirmationPasswordChange(user);
      }catch(err){
        throw new InternalServerErrorException("Une erreur est survenue lors de l'envoi de la confirmation de changement de mot de passe par e-mail.");
      }
      return {
        status: HttpStatus.CREATED,
        message: "modification reussis"
      }
    } catch (error) {
      throw new InternalServerErrorException("Une erreur est survenue.");
    }
  }



  async sendVerifyPassword(email: string): Promise<any> {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new HttpException("l'utilisateur n'existe pas", HttpStatus.NOT_FOUND);
    } else {
      const payload = {
        user_id: user.user_id,
        username: user.username,
        utility: 'passwordForgot',
      };
      const token = await this.jwtService.signAsync(payload, {
        expiresIn: '1h',
        secret: process.env.SECRET_JWT,
      });
      const updateUserTokenPassword: UpdateUserDto = {
        token_password_forget: token,
      };
      try{
        await this.usersService.update(user.user_id, updateUserTokenPassword, user);
        await this.mailService.sendUserConfirmationRegistration(user, token);
      }catch(err){
        throw new InternalServerErrorException("Une erreur est survenue.");
      }
      return {
        status: HttpStatus.CREATED,
        message: "Courriel envoyé avec succes"
      };
      
    }
  }
}
