import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Role } from 'src/decorators/constants';
import { IS_PUBLIC_KEY, IS_PUBLIC_REACTIVE_KEY } from 'src/decorators/public.decorator';

@Injectable()
export class ReactiveGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const isReactiveRole = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_REACTIVE_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);

        console.log(isReactiveRole)

        if (isReactiveRole) {
            return true;
        }

        const userReactive = context.switchToHttp().getRequest()['user'];
        console.log(userReactive)
        if (!userReactive) {
            return false; // L'utilisateur n'est pas authentifié
        }

        const userDeleted = userReactive.deleted;
        if (userDeleted && userReactive.role === Role.User) {
            return false; // L'utilisateur n'est pas reactive
        }
        return true
    }
    
}
