// google.strategy.ts
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';
import { googleOAUTH2 } from '../constants/constants';
import { UsersService } from 'src/modules/users/users.service';
import { Users } from 'src/entities/user.entity';
@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(private userService: UsersService) {
    super({
      clientID: googleOAUTH2.clientID,
      clientSecret: googleOAUTH2.clientSecret,
      callbackURL: googleOAUTH2.callbackURL,
      passReqToCallback: googleOAUTH2.passReqToCallback,
      scope: ['profile', 'email'], // Les scopes que vous souhaitez demander
    });
  }

  async validate(
    request: any,
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ) {
    // Vous pouvez enregistrer l'utilisateur dans la base de données ici
    // et retourner les informations de l'utilisateur
    const data = profile._json;
    const user = await this.userService.findOneByEmail(profile.emails[0].value);
    let returnuser: UserGoogle | Users = {};

    if (!user) {
      returnuser = {
        provider: profile.provider,
        google_access_token: accessToken,
        google_refresh_token: refreshToken ? undefined : '',
        google_id: data.sub,
        first_name: data.given_name,
        last_name: data.family_name,
        username: data.name,
        avatar: data.picture,
        email: profile.emails[0].value,
        email_verified: data.email_verified,
        local: data.local,
      };

      //this.userService.create(null, returnuser);
    } else {
      returnuser = user;
    }

    done(null, returnuser);
  }
}

export interface UserGoogle {
  provider?: string;
  google_access_token?: string;
  google_refresh_token?: string;
  google_id?: string;
  first_name?: string;
  last_name?: string;
  username?: string;
  avatar?: string;
  email?: string;
  email_verified?: boolean;
  local?: string;
}
