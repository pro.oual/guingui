// facebook.strategy.ts
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-facebook';
import { facebookOAUTH2 } from '../constants/constants';
import { Users } from 'src/entities/user.entity';
import { UsersService } from 'src/modules/users/users.service';
@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
  constructor(private userService: UsersService) {
    super({
      clientID: facebookOAUTH2.clientID,
      clientSecret: facebookOAUTH2.clientSecret,
      callbackURL: facebookOAUTH2.callbackURL,
      passReqToCallback: facebookOAUTH2.passReqToCallback,
      profileFields: ['id', 'emails', 'displayName'], // Les champs que vous souhaitez demander
    });
  }

  async validate(
    request: any,
    accessToken: string,
    refreshToken: string,
    profile: any,
    done,
  ) {
    // Vous pouvez enregistrer l'utilisateur dans la base de données ici
    // et retourner les informations de l'utilisateur
    const user = await this.userService.findOneByEmail(profile._json.email);
    let returnuser: UserFacebook | Users = {};

    if (!user) {
      returnuser = {
        provider: profile.provider,
        facebook_access_token: accessToken,
        facebook_refresh_token: refreshToken,
        facebook_id: profile.id,
        first_name: profile.name.givenName,
        last_name: profile.name.family_name,
        username: profile.displayName,
        email: profile.emails[0].value,
      };
     // this.userService.create(null, returnuser);
    } else {
      returnuser = user;
    }

    done(null, returnuser);
  }
}

export interface UserFacebook {
  provider?: string;
  facebook_access_token?: string;
  facebook_refresh_token?: string;
  facebook_id?: string;
  email?: string;
  last_name?: string;
  first_name?: string;
  username?: string;
  gender?: string;
}
