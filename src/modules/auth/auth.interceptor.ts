import { Injectable, NestInterceptor, ExecutionContext, CallHandler, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { classToPlain, instanceToPlain } from 'class-transformer';
import { Observable, throwError, pipe } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Users } from 'src/entities/user.entity';
import { CreateUserDto } from '../users/dto/create-user.dto';

@Injectable()

//TODO utiliser les interceptor pour verifier certaines donnees comme is_entreprise ? ou pour crypter un mot de passe
export class AuthInterceptor implements NestInterceptor {
  constructor() {}

  intercept(context: ExecutionContext, call$: CallHandler): Observable<any> {
    return call$.handle().pipe(map(
      data => 
      {
        console.log(data)
      instanceToPlain(data)
      console.log("2",data)

    }
      ));
    
    
  }
}
