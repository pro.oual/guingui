import { IsOptional, IsString } from 'class-validator';

export class NewPasswordDto {
  @IsString()
  password: string;

  @IsString()
  token: string;
}
