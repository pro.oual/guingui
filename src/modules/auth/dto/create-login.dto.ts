import { IsOptional, IsString } from 'class-validator';

export class CreateLoginDto {
  @IsString()
  @IsOptional()
  email: string;

  @IsString()
  @IsOptional()
  password: string;
}
