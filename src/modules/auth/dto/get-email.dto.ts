import { IsOptional, IsString } from 'class-validator';

export class GetEmailDto {
  @IsString()
  email: string;
}
