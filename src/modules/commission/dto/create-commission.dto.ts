import { IsNumber, IsOptional, IsString, IsDate } from "class-validator";

export class CreateCommissionDto {

  
    @IsNumber()
    montant: number;
  
    @IsString()
    @IsOptional()
    description: string;
  
    @IsDate()
    @IsOptional()
    created_at: Date;
  
    @IsDate()
    @IsOptional()
    updated_at: Date;
  
    @IsDate()
    @IsOptional()
    deleted_at: Date;
  
    @IsNumber()
    @IsOptional()
    amount_min: number;
  
    @IsNumber()
    @IsOptional()
    amount_max: number;

}
