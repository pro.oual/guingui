import { Module } from '@nestjs/common';
import { CommissionService } from './commission.service';
import { CommissionController } from './commission.controller';
import { Commission } from 'src/entities/commission.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports:[TypeOrmModule.forFeature([Commission])],
  controllers: [CommissionController],
  providers: [CommissionService],
})
export class CommissionModule {}
