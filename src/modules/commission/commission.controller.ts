import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe, HttpStatus, ValidationPipe } from '@nestjs/common';
import { CommissionService } from './commission.service';
import { CreateCommissionDto } from './dto/create-commission.dto';
import { UpdateCommissionDto } from './dto/update-commission.dto';
import { Roles } from 'src/decorators/roles.decorator';
import { Role } from 'src/decorators/constants';

@Controller('commission')
export class CommissionController {
  constructor(private readonly commissionService: CommissionService) {}

  @Roles(Role.Admin)
  @Post()
  create(@Body(new ValidationPipe()) createCommissionDto: CreateCommissionDto) {
    return this.commissionService.create(createCommissionDto);
  }

  @Roles(Role.Admin)
  @Get()
  findAll() {
    return this.commissionService.findAll();
  }

  @Roles(Role.Admin)
  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE }) ) id: string) {
    return this.commissionService.findOne(+id);
  }

  @Roles(Role.Admin)
  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateCommissionDto: UpdateCommissionDto) {
    return this.commissionService.update(+id, updateCommissionDto);
  }
  @Roles(Role.Admin)
  @Delete(':id')
  remove(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.commissionService.remove(+id);
  }
}
