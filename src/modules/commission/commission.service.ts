import { Injectable } from '@nestjs/common';
import { CreateCommissionDto } from './dto/create-commission.dto';
import { UpdateCommissionDto } from './dto/update-commission.dto';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Commission } from 'src/entities/commission.entity';
import { EntityManager, Repository } from 'typeorm';

@Injectable()
export class CommissionService {
  constructor(
    @InjectRepository(Commission)
    private commissionRepository: Repository<Commission>,
  ) {}

  async create(createCommissionDto: CreateCommissionDto) {
    return this.commissionRepository.save(createCommissionDto);
  }

  async findAll() {
    return this.commissionRepository.find();
  }

  async findOne(id: number) {
    return this.commissionRepository.findOneBy({
      commission_id: +id,
    });
  }

  async update(id: number, updateCommissionDto: UpdateCommissionDto) {
    return this.commissionRepository.update(+id, updateCommissionDto);
  }

  async remove(id: number) {
    return this.commissionRepository.delete({
      commission_id: +id,
    });
  }
}
