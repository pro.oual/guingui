import { Module } from '@nestjs/common';
import { DurationInfoServiceService } from './duration-info-service.service';
import { DurationInfoServiceController } from './duration-info-service.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DurationInfoService } from 'src/entities/duration-info-service.entity';

@Module({
  imports:[TypeOrmModule.forFeature([DurationInfoService])],
  controllers: [DurationInfoServiceController],
  providers: [DurationInfoServiceService],
})
export class DurationInfoServiceModule {}
