import { Test, TestingModule } from '@nestjs/testing';
import { DurationInfoServiceController } from './duration-info-service.controller';
import { DurationInfoServiceService } from './duration-info-service.service';

describe('DurationInfoServiceController', () => {
  let controller: DurationInfoServiceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DurationInfoServiceController],
      providers: [DurationInfoServiceService],
    }).compile();

    controller = module.get<DurationInfoServiceController>(DurationInfoServiceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
