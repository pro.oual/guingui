import { Transform } from "class-transformer";
import { IsInt, IsNotEmpty, IsOptional, IsString } from "class-validator";
import * as moment from "moment";
import { Service } from "src/entities/service.entity";

export class CreateDurationInfoServiceDto {
    
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    date_of_evenement: Date;
  
    @IsInt()
    number_of_hours: number;
  
    @IsInt()
    number_of_agent: number;
  
    @IsInt()
    number_of_guest: number;
  
    @IsString()
    @IsNotEmpty()
    place_of_service: string;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    datetime_of_start: Date;
  
    @Transform(({ value }) => moment(value, 'DD/MM/YY').toDate())

    datetime_of_end: Date;
  
    @IsString()
    @IsNotEmpty()
    place_of_start: string;
  
    @IsString()
    @IsNotEmpty()
    place_of_end: string;
  
    @IsInt()
    @IsNotEmpty()
    service_id: number;

    @IsNotEmpty()
    service: Service;
}
