import { PartialType } from '@nestjs/mapped-types';
import { CreateDurationInfoServiceDto } from './create-duration-info-service.dto';

export class UpdateDurationInfoServiceDto extends PartialType(CreateDurationInfoServiceDto) {}
