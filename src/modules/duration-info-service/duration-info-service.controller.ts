import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, ParseIntPipe } from '@nestjs/common';
import { DurationInfoServiceService } from './duration-info-service.service';
import { CreateDurationInfoServiceDto } from './dto/create-duration-info-service.dto';
import { UpdateDurationInfoServiceDto } from './dto/update-duration-info-service.dto';

@Controller('duration-info-service')
export class DurationInfoServiceController {
  constructor(private readonly durationInfoServiceService: DurationInfoServiceService) {}

  @Post()
  create(@Body() createDurationInfoServiceDto: CreateDurationInfoServiceDto) {
    return this.durationInfoServiceService.create(createDurationInfoServiceDto);
  }

  @Get()
  findAll() {
    return this.durationInfoServiceService.findwithRelation();
  }

  @Get(':id')
  findOne(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.durationInfoServiceService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string, @Body() updateDurationInfoServiceDto: UpdateDurationInfoServiceDto) {
    return this.durationInfoServiceService.update(+id, updateDurationInfoServiceDto);
  }

  @Delete(':id')
  remove(@Param('id',new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) id: string) {
    return this.durationInfoServiceService.remove(+id);
  }
}
