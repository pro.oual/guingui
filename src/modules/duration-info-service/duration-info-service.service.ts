import { Injectable } from '@nestjs/common';
import { CreateDurationInfoServiceDto } from './dto/create-duration-info-service.dto';
import { UpdateDurationInfoServiceDto } from './dto/update-duration-info-service.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DurationInfoService } from 'src/entities/duration-info-service.entity';
import { QuestionAnswer } from 'src/entities/question-answer.entity';
import { Repository } from 'typeorm';
import { CreateQuestionAnswerDto } from '../question-answer/dto/create-question-answer.dto';
import { UpdateQuestionAnswerDto } from '../question-answer/dto/update-question-answer.dto';

@Injectable()
export class DurationInfoServiceService {
  
  
  constructor(
    @InjectRepository(DurationInfoService)
    private durationInfoServiceRepository: Repository<DurationInfoService>
  
  ) {}
  create(createDurationInfoServiceDto: CreateDurationInfoServiceDto) {
    return this.durationInfoServiceRepository.save(createDurationInfoServiceDto)
  }

  findAll() {
    return this.durationInfoServiceRepository.find();
  }

  async findwithRelation(){
    const opts = await this.durationInfoServiceRepository.createQueryBuilder('dis')
    .leftJoinAndSelect("dis.service", "dissrv")
    //.where('chat.sender = :userId', { userId: user.user_id })
    //.orWhere('chat.receiver = :userId',{ userId: user.user_id})
    .getMany();
    return opts;
  }

  findOne(id: number) {
    return this.durationInfoServiceRepository.findBy({
      DurationInfoService_id: +id
    });
  }

  update(id: number, updateQuestionAnswerDto: UpdateQuestionAnswerDto) {
    return this.durationInfoServiceRepository.update(
      +id,
      updateQuestionAnswerDto
    );
  }

  remove(id: number) {
    return this.durationInfoServiceRepository.delete(
      +id
    );
  }
  
  
  
}
