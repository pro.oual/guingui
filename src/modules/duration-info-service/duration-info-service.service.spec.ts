import { Test, TestingModule } from '@nestjs/testing';
import { DurationInfoServiceService } from './duration-info-service.service';

describe('DurationInfoServiceService', () => {
  let service: DurationInfoServiceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DurationInfoServiceService],
    }).compile();

    service = module.get<DurationInfoServiceService>(DurationInfoServiceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
