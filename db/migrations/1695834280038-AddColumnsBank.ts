import { MigrationInterface, QueryRunner } from "typeorm";

export class AddColumnsBank1695834280038 implements MigrationInterface {
    name = 'AddColumnsBank1695834280038'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`Users\` (\`user_id\` int NOT NULL AUTO_INCREMENT, \`first_name\` varchar(255) NOT NULL, \`last_name\` varchar(255) NOT NULL, \`username\` varchar(255) NOT NULL, \`phone_number\` varchar(255) NOT NULL, \`number_address\` varchar(10) NOT NULL, \`address\` text NOT NULL, \`zip_code\` varchar(10) NOT NULL, \`city\` varchar(255) NOT NULL, \`genre\` varchar(10) NOT NULL, \`date_of_birth\` date NOT NULL, \`is_active\` tinyint NOT NULL DEFAULT 1, \`email\` varchar(255) NULL, \`password\` varchar(50) NULL, \`last_seen\` datetime NULL, \`is_blocked\` tinyint NOT NULL DEFAULT 0, \`block_reason\` varchar(255) NULL, \`avatar\` blob NULL, \`token_password_forget\` text NULL, \`email_verified_at\` datetime NULL, \`general_conditions\` tinyint NOT NULL DEFAULT 0, \`provider\` varchar(50) NOT NULL, \`iban\` varchar(255) NOT NULL, \`bic\` varchar(255) NOT NULL, \`bank_name\` varchar(255) NULL, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deleted_at\` datetime(6) NULL, \`is_entreprise\` tinyint NOT NULL DEFAULT 0, \`role\` varchar(50) NOT NULL, \`google_id\` varchar(255) NULL, \`google_access_token\` varchar(255) NULL, \`google_refresh_token\` varchar(255) NULL, \`facebook_id\` varchar(255) NULL, \`facebook_access_token\` varchar(255) NULL, \`facebook_refresh_token\` varchar(255) NULL, PRIMARY KEY (\`user_id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`Bank\` (\`bank_id\` int NOT NULL AUTO_INCREMENT, \`iban\` varchar(255) NULL, \`bank_bic\` varchar(255) NULL, \`userIdUserId\` int NULL, UNIQUE INDEX \`IDX_d4e0298aeb86bf658b4ac52fe7\` (\`userIdUserId\`), UNIQUE INDEX \`REL_d4e0298aeb86bf658b4ac52fe7\` (\`userIdUserId\`), PRIMARY KEY (\`bank_id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`Bank\` ADD CONSTRAINT \`FK_d4e0298aeb86bf658b4ac52fe76\` FOREIGN KEY (\`userIdUserId\`) REFERENCES \`Users\`(\`user_id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`Bank\` DROP FOREIGN KEY \`FK_d4e0298aeb86bf658b4ac52fe76\``);
        await queryRunner.query(`DROP INDEX \`REL_d4e0298aeb86bf658b4ac52fe7\` ON \`Bank\``);
        await queryRunner.query(`DROP INDEX \`IDX_d4e0298aeb86bf658b4ac52fe7\` ON \`Bank\``);
        await queryRunner.query(`DROP TABLE \`Bank\``);
        await queryRunner.query(`DROP TABLE \`Users\``);
    }

}
